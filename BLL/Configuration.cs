﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Configuration
    {
        public class Bazzar
        {
            public static string AccessToken
            {


                get { return "l7SQ12N8g0d3EqWbT7sephSQreFrRg"; }

            }

            public static string RefreshToken
            {


                get { return "az3znNLooJ737yK0chJHwJlYlFEjdL"; }


            }

            public static string PakageName
            {
                get { return "ir.koodaksalam.android"; }
            }






        }

        public class PaymentInfo
        {
            public class Pasargad
            {
                private static readonly PaymentInfo instance = new PaymentInfo();
                private static readonly string _privateKey = "<RSAKeyValue><Modulus>vFSYV9Smz3nmXSfAprWIcHrMl8BHSCaBmitRrv9CZQprHzjCcVCr25wdn2PLCnPNwIrOVPUYXL7M7eRenTIcBnSbs7/7gWsADKsov0bY0box5J+6keBHYlbmc2rL6eUzxRRlI4Ecr6VW9DGE5c02yDwGOBVaYgnjoB8mypaOKZM=</Modulus><Exponent>AQAB</Exponent><P>9l7WUuQVFgiseC7sQ7B95rYo9IM03AAxwzP2r0mVPFNOssKhvzqzXEVRU3qsNPCAjaqI/x+kKn6qnw16f0TjpQ==</P><Q>w7EDp7P/kYPR552ZMAVeAX69fFEEDMdblQ9UZ2qdX5cfjVGkXRCQkMUV7RmWePQ3jhHEgeQy6ve18n+lZq3y1w==</Q><DP>t0KSMDRijvJCwF8+9ZbZ//xt+HuEnUUzvtzvWTHl5WqedpAaWrGiazdQqETuxa5EIZzBrOxvZAC2j/sprHOk1Q==</DP><DQ>bTrQJHq3S5z9vD/essw8Ja99yvdZwFxCxVgSL8t13lZ3WSVaDkxhtT0dOQQQVqFSpS1Lt4kN1tO/roULaN3tbw==</DQ><InverseQ>V7rofOBbbvp2Mr7/rgsRGMi0E8waQ6flDFCv78ANPU0rwQYnvk2kSNZMwKACd4ithp4W4MpSFaw1KCGo7W21hQ==</InverseQ><D>c1e0Pgala2iTJ/aSzQddWtY6u64tegjrU0q4ql73gP6SgPq4S8JGnyfMFLR/xBUMrKHWoL3Df/nzdLdrIwdvlY3S3TtBG3nRKhMJlviy7q7/LYX32gErL34M20Q1ijG+UwmRxSZcBIdJiwB9wMoL7oh3/M2A+3Wb+1J0DNBBMPE=</D></RSAKeyValue>";
                private static readonly string _merchantCode = "4318731";
                private static readonly string _terminalCode = "1489042";


                public static string PrivateKey
                {
                    get
                    {
                        return _privateKey;
                    }
                }
                public static string MerchantCode
                {
                    get
                    {
                        return _merchantCode;
                    }
                }
                public static string TerminalCode
                {
                    get
                    {
                        return _terminalCode;
                    }
                }
            }

            public class PasargadGheseland
            {
                private static readonly PaymentInfo instance = new PaymentInfo();
                private static readonly string _privateKey = "<RSAKeyValue><Modulus>ipH6t2NtKcjWcESS+DVxf23h9okkeBRWitBjMMKrKRMyUqMAgX9oW4OLtT27QHPu8oWgi38GAGBhRqzRmaRRTZdMEaIrIJphVuHtQGmMT5ivnVF6grPmMX5ZxmJHBmE11oka++EmRvuzzRDnEUDTv8Kz7eeVFsJQ4up0h7Mnn+s=</Modulus><Exponent>AQAB</Exponent><P>w+/mG0fapsQub0pR6R9iv1ND8djB5skRRPW0SHIz5MIEVorOBXEIlsEfmjqKhUApgRa+s5b6B2JAB7YcRwO0NQ==</P><Q>tQw7z8W4idj1QsNDmPy0ZMDwCezmuQ5z4rOA+Tbf/rJIdmB93cOrdKNBwRqu1GPjvLG0cGaLiRj7LZ2fqWBHnw==</Q><DP>GKc4troFwvHwPcSGJeTzEcuyQt3ZF+a4q4qW0n68FM5sqU7xuTGNuRo3lbpD75wxHLL177bRYthz4gSB7lWC+Q==</DP><DQ>hp+eKhU/72CgnxBLR4tBKFo/I2JA/gI9MamAkc+4J8+yCjQd47UH3Sc4UbrhnZTZVwpcvefCgiNNQtIeUvgAyQ==</DQ><InverseQ>Dfbi5jh8QOcaOXsiM+UxilaGVo2rRoF124zVDIaAK9TqCllf9/f0C9MhOMmcrhwT+yw21aKkBMbhHIKP1NJmKg==</InverseQ><D>POtome8wMsqE296J7m0wdKFOUiCA0/m9AlITCYDL1scFvKbEtinZK0JtzURSakeNqTluF2XEsa2tlnCrxSwkQZ6h7fVHHMB+7TIC1xPgljFNkcWIOMfQm5uRDHyWc+EtkOwW2vx9k+fqQqQUdiLBKW6eR95tZ/vMvmo2/N2nPSE=</D></RSAKeyValue>";
                private static readonly string _merchantCode = "4432838";
                private static readonly string _terminalCode = "1609695";


                public static string PrivateKey
                {
                    get
                    {
                        return _privateKey;
                    }
                }
                public static string MerchantCode
                {
                    get
                    {
                        return _merchantCode;
                    }
                }
                public static string TerminalCode
                {
                    get
                    {
                        return _terminalCode;
                    }
                }
            }


        }

        public class Sms
        {
            public class Armaghan
            {
                public static string PhoneLine1
                {
                    get { return "500041281"; }
                }
                public static string PhoneLine2
                {
                    get { return "5000412810"; }
                }

                public static string UserName
                {
                    get { return "989125434098"; }
                }


                public static string PassWord
                {
                    get { return "0184181"; }
                }
            }
        }


    }
}
