﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class Brand
    {

        public int ID { get; set; }
        public string BrandName_FA { get; set; }
        public string BrandName_EN { get; set; }
        public string BrandImage { get; set; }
        public int BrandPriority { get; set; }
        public int BrandIsAvtive { get; set; }

    }
}
