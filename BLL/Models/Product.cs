﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class Product
    {

        public int ID { get; set; }
        public int SubCategoryID { get; set; }
        public int BrandID { get; set; }
        public short ProductGender { get; set; }
        public string ProductCode { get; set; }
        public string ProductName_FA { get; set; }
        public string ProductName_EN { get; set; }
        public string ProductDesc { get; set; }
        public string LastPrice { get; set; }
        public string ProductMainImage { get; set; }
        public string Product3DImage { get; set; }
        public int ProductPriority { get; set; }
        public DateTime RegDate { get; set; }


    }
}
