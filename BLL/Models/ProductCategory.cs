﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class ProductCategory
    {

        public int ID { get; set; }
        public string CategoryName_FA { get; set; }

        public string CategoryName_EN { get; set; }

        public int CategoryPriority { get; set; }

        public DateTime RegDate { get; set; }

 
    }
}
