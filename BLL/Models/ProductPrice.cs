﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class ProductPrice
    {


        public Guid ID { get; set; }
        public string Price { get; set; }
        public int ProductID { get; set; }
        public string CashBack { get; set; }
        public int SalePercent { get; set; }
        public string LastPrice { get; set; }
        public DateTime RegDate { get; set; }

    }
}
