﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class ProductSubCategory
    {

        public int ID { get; set; }

        public int CategoryID { get; set; }
        public string SubCategoryName_FA { get; set; }

        public string SubCategoryName_EN { get; set; }

        public int SubCategoryPriority { get; set; }

        public DateTime RegDate { get; set; }


    }
}
