﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class ShamsiTime
    {


        public string FullStringTime { get; set; }
        public string FullNumeralTime { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public string Day { get; set; }
        public int WeekDay { get; set; }
        public string MonthName { get; set; }
        public string WeekDayName { get; set; }

    }
}
