﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class User
    {


        public Guid ID { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string ImageUrl { get; set; }
        public string FullName { get; set; }
        public bool Gender { get; set; }
        public string Birthday { get; set; }
        public string NationalCode { get; set; }
        public string Password { get; set; }
        public bool IsNewsletter { get; set; }
        public int MobileRegisterd { get; set; }
        public string RegCode { get; set; }
        public bool IsActive { get; set; }
        public DateTime LastLoginDate { get; set; }
        public DateTime RegDate { get; set; }


    }
}

