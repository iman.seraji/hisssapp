﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="AddEditBrands.aspx.cs" Inherits="HisssApp.Admin.AddEditBrands" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SiteMap" runat="server">


    <li><a href="AdminDashboard.aspx"><i class="fa fa-dashboard"></i>داشبرد</a></li>

    <li><a href="StoryList.aspx">ویرایش برند ها</a></li>
    <li class="active">ویرایش برند ها</li>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    ویرایش برند ها
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">

    <table>
        <tr>
            <td colspan="2">


                <asp:Label ID="lblDelete" runat="server" ForeColor="Red" Visible="false" Text="کاربر گرامی درصورتی که برای حذف مطمئن هستید دگمه حذف را بزنید .!!!!!"></asp:Label>
                <br />
                <br />
                <br />
                <br />


            </td>
        </tr>
        <tr>
            <td>نام فارسی : </td>
            <td>
                <asp:TextBox ID="txtBrandName_FA" runat="server" Width="630px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="لطفا نام فارسی را وارد نمایید ." ControlToValidate="txtBrandName_FA"></asp:RequiredFieldValidator>

            </td>
        </tr>
        <tr>
            <td>نام انگلیسی : </td>
            <td>
                <asp:TextBox ID="txtBrandName_EN" runat="server" Width="630px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="لطفا نام انگلیسی را وارد نمایید ." ControlToValidate="txtBrandName_EN"></asp:RequiredFieldValidator>

            </td>
        </tr>

        <tr>
            <td>تصویر  : </td>
            <td>
                <asp:MultiView ID="mvImage" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View5" runat="server">
                        <asp:FileUpload ID="fuImage" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="لطفا تصویر را وارد نمایید ." ControlToValidate="fuImage"></asp:RequiredFieldValidator>

                    </asp:View>
                    <asp:View ID="View6" runat="server">

                        <table>
                            <tr>
                                <td>
                                    <asp:Image ID="imgImage" Width="115" Height="60" runat="server" />
                                </td>
                                <td style="width: 20px"></td>
                                <td>
                                    <asp:LinkButton ID="lbImage" runat="server" ValidationGroup="Delete" OnClick="lbImage_Click">حذف فایل</asp:LinkButton></td>
                            </tr>
                        </table>

                    </asp:View>
                </asp:MultiView>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br /><br />
                 <asp:Button ID="btnDelete" runat="server" Text="حذف" Width="90px" Visible="false"   ValidationGroup="Delete" class="btn btn-info" OnClick="btnDelete_Click" />
                <asp:Button ID="btnSubmit" runat="server" Text="ثبت" Width="90px" class="btn btn-success" OnClick="btnSubmit_Click" />
                <asp:Button ID="Button1" runat="server" Text="انصراف" Width="90px" PostBackUrl="ManageBrands.aspx" ValidationGroup="Cancel" class="btn btn-danger" />

            </td>
        </tr>
        <tr>
            <td colspan="2">

                <asp:Label ID="lblMessage" runat="server" ForeColor="red" Text=""></asp:Label>

                <asp:HiddenField ID="hfBrandID" runat="server" />

            </td>
        </tr>
    </table>

</asp:Content>
