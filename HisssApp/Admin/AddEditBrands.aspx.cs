﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using HisssApp.Services;

namespace HisssApp.Admin
{
    public partial class AddEditBrands : System.Web.UI.Page
    {

        HisssAppDBDataContext HisssAppDB = new HisssAppDBDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {

                

                if (Request.QueryString["BID"] != null)
                {



                    hfBrandID.Value = Request.QueryString["BID"].ToString();
                    int intBrandID = Convert.ToInt32(Request.QueryString["BID"]);

                    var qrBrand = (from BR in HisssAppDB.tblBrands   where BR.ID == intBrandID select BR).First();

                 

                    txtBrandName_FA.Text = qrBrand.BrandName_FA;
                    txtBrandName_EN.Text = qrBrand.BrandName_EN;



                    if (!string.IsNullOrEmpty(qrBrand.BrandImage))
                    {
                        mvImage.ActiveViewIndex = 1;
                        imgImage.ImageUrl = qrBrand.BrandImage;
                    }
                    else
                    {
                        mvImage.ActiveViewIndex = 0;
                    }

                  

                     if (Request.QueryString["Mode"] == "Delete")
                     {

                         txtBrandName_FA.Enabled = false;
                         txtBrandName_EN.Enabled = false;
                         fuImage.Enabled = false;
                         lbImage.Visible = false;
                         lblDelete.Visible = true;
                         btnDelete.Visible = true;
                         btnSubmit.Visible = false;

                         
                     }
                     else
                     {
                         txtBrandName_FA.Enabled = true;
                         txtBrandName_EN.Enabled = true;
                         fuImage.Enabled = true;
                         lbImage.Visible = true;
                         lblDelete.Visible = false;
                         btnDelete.Visible = false;
                         btnSubmit.Visible = true;
                     }




                   

                }
                else
                {
                    hfBrandID.Value = "No";
                }



            }



        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {



           
            bool blnBrandImage = false;






            if (hfBrandID.Value != "No")
            {

                var qrBrandEdit = (from BE in HisssAppDB.tblBrands where BE.ID == Convert.ToInt32(hfBrandID.Value.ToString()) select BE).First();
                                
                qrBrandEdit.BrandName_FA = txtBrandName_FA.Text;
                qrBrandEdit.BrandName_EN = txtBrandName_EN.Text;

                
                if (fuImage.HasFiles)
                {
                    string strFileName = UtilityServices.GenerateFileName(fuImage.PostedFile.FileName);
                    qrBrandEdit.BrandImage = "http://www.HisssApp.com/files/Brand_Images/" + strFileName;

                    blnBrandImage = UtilityServices.SaveFile(Server.MapPath("~/Files/Brand_Images/"), fuImage, strFileName, ".jpg,.jpeg,.png");
                }
                else
                {


                    if (qrBrandEdit.BrandImage != null && qrBrandEdit.BrandImage.ToString() != "")
                    {
                        blnBrandImage = true;
                    }

                    else
                    {
                        blnBrandImage = false;

                    }


                }
               



                if (blnBrandImage)
                {


                    HisssAppDB.SubmitChanges();

                    lblMessage.Text = "اطلاعات با موفقیت ثبت شد .";
                    Response.Redirect("ManageBrands.aspx");
                }
                else
                {
                    lblMessage.Text = "فرمت فابلهای وارد شده مناسب نیست";
                }


            }
            else
            {

                string strFileName = UtilityServices.GenerateFileName(fuImage.PostedFile.FileName);
                blnBrandImage = UtilityServices.SaveFile(Server.MapPath("~/Files/Brand_Images/"), fuImage,strFileName, ".jpg,.jpeg,.png");


                if (blnBrandImage)
                {


                    tblBrand BrandObject = new tblBrand()
                    {

                        BrandName_FA = txtBrandName_FA.Text,
                        BrandName_EN = txtBrandName_EN.Text,
                        BrandImage = "http://www.HisssApp.com/files/Brand_Images/" + strFileName,
                                          
                    };

                    HisssAppDB.tblBrands.InsertOnSubmit(BrandObject);
                    HisssAppDB.SubmitChanges();
                    lblMessage.Text = "اطلاعات با موفقیت ثبت شد .";

                    Response.Redirect("ManageBrands.aspx");
                }

                else
                {
                    lblMessage.Text = "فرمت فابلهای وارد شده مناسب نیست";
                }







            }





        }

        protected void lbImage_Click(object sender, EventArgs e)
        {

            try
            {

                string strImage ;

                var qrDelete = (from BD in HisssAppDB.tblBrands where BD.ID == Convert.ToInt32(hfBrandID.Value.ToString()) select BD).First();

                strImage = qrDelete.BrandImage;

                strImage = "~" + strImage.Remove(0, 23);

                qrDelete.BrandImage = "";

                HisssAppDB.SubmitChanges();

                UtilityServices.DeleteFile(Server.MapPath(strImage));


                mvImage.ActiveViewIndex = 0;
                imgImage.ImageUrl = "";

            }
            catch (Exception ex)
            {

                lblMessage.Text = ex.Message;

            }


        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {



            try
            {

                string strImage;

                var qrDelete = (from BD in HisssAppDB.tblBrands where BD.ID == Convert.ToInt32(hfBrandID.Value.ToString()) select BD).First();

                strImage = qrDelete.BrandImage;

                strImage = "~" + strImage.Remove(0, 23);
        

                HisssAppDB.tblBrands.DeleteOnSubmit(qrDelete);

                HisssAppDB.SubmitChanges();

                UtilityServices.DeleteFile(Server.MapPath(strImage));


                Response.Redirect("ManageBrands.aspx");

            }
            catch (Exception ex)
            {

                lblMessage.Text = ex.Message;

            }




        }
    }
}