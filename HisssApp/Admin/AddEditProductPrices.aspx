﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="AddEditProductPrices.aspx.cs" Inherits="HisssApp.Admin.AddEditProductPrices" EnableEventValidation="false" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SiteMap" runat="server">


    <li><a href="AdminDashboard.aspx"><i class="fa fa-dashboard"></i>داشبرد</a></li>

    <li><a href="ManageProducts.aspx">مدیریت محصولات</a></li>
    <li class="active">لیست قیمت های محصول</li>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    لیست قیمت های محصول
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">

    <table>
        <tr>
            <td colspan="2">



                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>


                        <table style="vertical-align: middle; text-align: center">
                            <tr>
                                <td>قیمت</td>
                                <td>درصد بازگشت</td>
                                <td>درصد تخفیف</td>
                                <td>قیمت نهایی</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td style="padding: 8px;">
                                    <asp:TextBox ID="txtPrice" runat="server" ValidationGroup="Price" AutoPostBack="True" OnTextChanged="txtPrice_TextChanged" TextMode="Number"></asp:TextBox>

                                </td>
                                <td style="padding: 8px;">
                                    <asp:TextBox ID="txtCashBack" runat="server" Text="0" ValidationGroup="Price" OnTextChanged="txtPrice_TextChanged" TextMode="Number" MaxLength="2" AutoPostBack="True"></asp:TextBox>
                                </td>
                                <td style="padding: 8px;">
                                    <asp:TextBox ID="txtSalePercent" runat="server" ValidationGroup="Price" OnTextChanged="txtPrice_TextChanged" TextMode="Number" MaxLength="2" AutoPostBack="True">0</asp:TextBox>
                                </td>
                                <td style="padding: 8px;">
                                    <asp:TextBox ID="txtLastPrice" runat="server" ValidationGroup="Price" Enabled="False" MaxLength="2" TextMode="Number"></asp:TextBox>
                                </td>
                                <td style="padding: 8px;">
                                    <asp:ImageButton ID="ibtnSubmitPrice" runat="server" ImageUrl="~/Images/ADD.png" Width="25px" Height="25" OnClick="ibtnSubmitPrice_Click" ValidationGroup="Price" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="لطفا قیمت را وارد نمایید ." ControlToValidate="txtPrice" ValidationGroup="Price"></asp:RequiredFieldValidator>
                                    <asp:Label ID="lblPriceErr" runat="server" ForeColor="red" Text=""></asp:Label>
                                    <asp:HiddenField ID="hfPrice" runat="server" />
                                </td>
                            </tr>
                     
                            <tr>
                                <td colspan="5">



                                    <asp:GridView ID="grvPrices" runat="server" AutoGenerateColumns="False" GridLines="None" class="table table-bordered table-hover" AllowSorting="True" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" OnRowCommand="grvPrices_RowCommand">
                                        <AlternatingRowStyle BackColor="White" />
                                        <Columns>
                                            <asp:BoundField DataField="Price" HeaderText="قیمت" SortExpression="Price" />
                                            <asp:BoundField DataField="CashBack" HeaderText="درصد بازگشت" SortExpression="CashBack" />
                                            <asp:BoundField DataField="SalePercent" HeaderText="درصد تخفیف" SortExpression="SalePercent" />
                                            <asp:BoundField DataField="LastPrice" HeaderText="قیمت نهایی" SortExpression="LastPrice" />
                                            <asp:BoundField DataField="RegDate" HeaderText="تاریخ ثبت" SortExpression="RegDate" />
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>




                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="PriceDelete" CommandArgument='<%# Eval("ID")%>' OnClientClick="return confirm('برای حذف این رکورد اطمینان دارید ؟');">
                                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Delete-Icone-red.png" Width="30px" Height="30px" />
                                                    </asp:LinkButton>


                                                </ItemTemplate>
                                            </asp:TemplateField>


                                        </Columns>
                                        <EmptyDataTemplate>
                                            هیچ قیمتی درج نشده
                                       
                                        </EmptyDataTemplate>
                                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#50af00" Font-Bold="True" ForeColor="White" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                        <SortedAscendingCellStyle BackColor="#FDF5AC" />
                                        <SortedAscendingHeaderStyle BackColor="#4D0000" />
                                        <SortedDescendingCellStyle BackColor="#FCF6C0" />
                                        <SortedDescendingHeaderStyle BackColor="#820000" />
                                    </asp:GridView>







                                </td>
                            </tr>
                        </table>



                    </ContentTemplate>
                </asp:UpdatePanel>







            </td>
        </tr>

        <tr>
            <td></td>
        </tr>



        <tr>
            <td>
                <br />
                <br />
                
                <asp:Button ID="Button1" runat="server" Text="بازگشت" Width="90px" PostBackUrl="ManageProducts.aspx"   class="btn btn-success"  />

            </td>
        </tr>
        <tr>
            <td>

                <asp:Label ID="lblMessage" runat="server" ForeColor="red" Text=""></asp:Label>

                <asp:HiddenField ID="hfBrandID" runat="server" />

            </td>
        </tr>
    </table>

</asp:Content>
