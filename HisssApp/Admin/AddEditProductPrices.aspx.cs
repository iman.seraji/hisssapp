﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Models;

namespace HisssApp.Admin
{
    public partial class AddEditProductPrices : System.Web.UI.Page
    {
        HisssAppDBDataContext HisssAppDB = new HisssAppDBDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {



            if (!IsPostBack)
            {



                if (Request.QueryString["PID"] != null)
                {
                    FillPrices();


                }
                else
                {
                    Response.Redirect("ManageProducts.aspx");
                }




            }
            
        }


        void FillPrices()
        {

            int intProductID = Convert.ToInt32(Request.QueryString["PID"]);

            var Prices = (from PR in HisssAppDB.tblProduct_prices
                          where PR.ProductID == intProductID
                          orderby PR.RegDate descending
                          select PR).ToList();

            grvPrices.DataSource = Prices;
            grvPrices.DataBind();
        }



        protected void ibtnSubmitPrice_Click(object sender, ImageClickEventArgs e)
        {


            if (Convert.ToInt32(txtPrice.Text) > 100)
            {


                tblProduct_price PriceObject = new tblProduct_price()
                {
                    ID = Guid.NewGuid(),
                    Price = txtPrice.Text,
                    ProductID = Convert.ToInt32(Request.QueryString["PID"].ToString()),
                    SalePercent = Convert.ToInt32(txtSalePercent.Text),
                    CashBack = txtCashBack.Text,
                    LastPrice = txtLastPrice.Text,
                    RegDate = DateTime.Now

                };

                HisssAppDB.tblProduct_prices.InsertOnSubmit(PriceObject);


               


                var qrPriceEdit =
                    (from PR in HisssAppDB.tblProducts
                        where PR.ID == Convert.ToInt32(Request.QueryString["PID"].ToString())
                        select PR).FirstOrDefault();

                qrPriceEdit.LastPrice = txtLastPrice.Text;




                HisssAppDB.SubmitChanges();

            }
            else
            {


                lblPriceErr.Text = "حداقل مبلغ قابل قبول 100 تومان می باشد .";


            }




       





            FillPrices();


        }


        protected void txtPrice_TextChanged(object sender, EventArgs e)
        {

            if (txtSalePercent.Text == "")
            {
                txtSalePercent.Text = "0";
            }

            if (txtCashBack.Text == "")
            {
                txtCashBack.Text = "0";
            }
            if (txtPrice.Text == "")
            {
                txtPrice.Text = "0";
            }

            int intPrice = Convert.ToInt32(txtPrice.Text);
            int intCashBack = Convert.ToInt32(txtCashBack.Text);
            int intSale = Convert.ToInt32(txtSalePercent.Text);
            int intLastPrice = 0;

            if (intSale != 0)
            {
                intLastPrice = intPrice - (intPrice * intSale) / 100;
            }
            else
            {
                intLastPrice = intPrice;
            }





            txtLastPrice.Text = intLastPrice.ToString();
        }



        protected void grvPrices_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "PriceDelete")
            {
                Guid DeleteID = new Guid(e.CommandArgument.ToString());

                var qrDelete = (from PP in HisssAppDB.tblProduct_prices where PP.ID == DeleteID select PP).FirstOrDefault();

                if (qrDelete != null)
                {

                    HisssAppDB.tblProduct_prices.DeleteOnSubmit(qrDelete);

                    HisssAppDB.SubmitChanges();

                }

            }



            FillPrices();


        }
    }
}