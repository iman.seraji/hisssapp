﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="AddEditProducts.aspx.cs" Inherits="HisssApp.Admin.AddEditProducts" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SiteMap" runat="server">


    <li><a href="AdminDashboard.aspx"><i class="fa fa-dashboard"></i>داشبرد</a></li>

    <li><a href="ManageProducts.aspx">مدیریت محصولات</a></li>
    <li class="active">جزئیات محصول</li>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    جزئیات محصول
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">

    <table style="padding: 10px;">


        <tr>
            <td>برند محصول : </td>
            <td>
                <asp:DropDownList ID="drpBrand" Width="250px" runat="server" class="form-control"></asp:DropDownList></td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>


                        <table width="100%">
                            <tr>
                                <td>گروه اصلی : </td>
                                <td>
                                    <asp:DropDownList ID="drpCat" Width="250px" runat="server" class="form-control" AutoPostBack="True" OnSelectedIndexChanged="drpCat_SelectedIndexChanged"></asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td>زیر گروه : </td>
                                <td>
                                     <asp:DropDownList ID="drpSubCat" Width="250px" runat="server" class="form-control"></asp:DropDownList></td>
                            </tr>
                        </table>


                    </ContentTemplate>
                </asp:UpdatePanel>





            </td>
        </tr>
        <tr>
            <td>جنسیت  : </td>
            <td>
                <asp:CheckBoxList ID="cblGenders" runat="server" RepeatDirection="Horizontal">
                </asp:CheckBoxList>




            </td>
        </tr>
        <tr>
            <td>کد محصول : 
                <td>
                    <asp:TextBox ID="txtProductCode" runat="server" Width="630px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="لطفا کد محصول را وارد نمایید ." ControlToValidate="txtProductCode"></asp:RequiredFieldValidator>

                </td>
        </tr>
        <tr>
            <td>نام فارسی : </td>
            <td>
                <asp:TextBox ID="txtProductName_FA" runat="server" Width="630px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="لطفا نام فارسی را وارد نمایید ." ControlToValidate="txtProductName_FA"></asp:RequiredFieldValidator>

            </td>
        </tr>
        <tr>
            <td>نام انگلیسی : </td>
            <td>
                <asp:TextBox ID="txtProductName_EN" runat="server" Width="630px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="لطفا نام انگلیسی را وارد نمایید ." ControlToValidate="txtProductName_EN"></asp:RequiredFieldValidator>

            </td>
        </tr>

        <tr>
            <td>توضیحات : </td>
            <td>

                <asp:TextBox ID="txtProductDesc" runat="server" Height="255px" TextMode="MultiLine" Width="630px"></asp:TextBox>


           


            </td>

        </tr>




        <tr>
            <td colspan="2">
                <hr />
            </td>

        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td></td>
        </tr>



        <tr>
            <td>تصویر اصلی : </td>
            <td>


                <asp:MultiView ID="mvImageMain" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View5" runat="server">
                        <asp:FileUpload ID="fuImageMain" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="لطفا تصویر اصلی را وارد نمایید ." ControlToValidate="fuImageMain"></asp:RequiredFieldValidator>

                    </asp:View>
                    <asp:View ID="View6" runat="server">

                        <table>
                            <tr>
                                <td>
                                    <asp:Image ID="imgImageMain" Width="70" Height="70" runat="server" />
                                </td>
                                <td style="width: 20px"></td>
                                <td>
                                    <asp:LinkButton ID="lbImageMain" runat="server" ValidationGroup="Delete" OnClick="lbImageMain_Click">حذف فایل</asp:LinkButton></td>
                            </tr>
                        </table>

                    </asp:View>





                </asp:MultiView>




            </td>
        </tr>
        <tr>
            <td>تصویر سه بعدی : </td>
            <td>



                <asp:MultiView ID="mvImage3D" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View7" runat="server">
                        <asp:FileUpload ID="fuImage3D" runat="server" />


                    </asp:View>
                    <asp:View ID="View8" runat="server">

                        <table>
                            <tr>
                                <td>
                                    <asp:Image ID="imgImage3D" Width="70" Height="70" runat="server" />
                                </td>
                                <td style="width: 20px"></td>
                                <td>
                                    <asp:LinkButton ID="lbImage3D" runat="server" ValidationGroup="Delete" OnClick="lbImage3D_Click">حذف فایل</asp:LinkButton></td>
                            </tr>
                        </table>

                    </asp:View>




                </asp:MultiView>



            </td>
        </tr>
        <tr>

            <td colspan="2">

                <hr />

                <asp:Button ID="btnSubmit" runat="server" Text="ثبت" Width="60px" OnClick="btnSubmit_Click" class="btn btn-success" />
                <asp:Button ID="btnDelete" runat="server" Text="حذف" Width="90px" Visible="false" ValidationGroup="Delete" class="btn btn-info" OnClick="btnDelete_Click" />
                <asp:Button ID="Button1" runat="server" Text="انصراف" Width="60px" PostBackUrl="ManageProducts.aspx" ValidationGroup="Cancel" class="btn btn-danger" />

            </td>
        </tr>

        <tr>

            <td colspan="2">

                <asp:Label ID="lblMessage" runat="server" ForeColor="red" Text=""></asp:Label>

                <asp:HiddenField ID="hfProductID" runat="server" />

            </td>
        </tr>
    </table>

</asp:Content>

