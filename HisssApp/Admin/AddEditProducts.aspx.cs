﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Models;
using HisssApp.Services;




using System.Data;

using System.Web.Hosting;

namespace HisssApp.Admin
{
    public partial class AddEditProducts : System.Web.UI.Page
    {
        HisssAppDBDataContext HisssAppDB = new HisssAppDBDataContext();






        protected void Page_Load(object sender, EventArgs e)
        {



            if (!IsPostBack)
            {

                Session["Price"] = null;
                FillCategories();
                FillBrands();
                FillGenders();
                FillSubCategories(Convert.ToInt32(drpCat.SelectedValue));
               
                if (Request.QueryString["PID"] != null)
                {

                   

                    hfProductID.Value = Request.QueryString["PID"].ToString();
                    int intProductID = Convert.ToInt32(Request.QueryString["PID"]);
                    FillGenders(intProductID);
                    var qrProduct = (from PR in HisssAppDB.tblProducts where PR.ID == intProductID select PR).First();

                    drpCat.SelectedValue = qrProduct.SubCategoryID.ToString();

                    drpBrand.SelectedValue = qrProduct.BrandID.ToString();

                    txtProductCode.Text = qrProduct.ProductCode;
                    txtProductName_FA.Text = qrProduct.ProductName_FA;
                    txtProductName_EN.Text = qrProduct.ProductName_EN;
                    txtProductDesc.Text = qrProduct.ProductDesc;





                    if (!string.IsNullOrEmpty(qrProduct.ProductMainImage))
                    {
                        mvImageMain.ActiveViewIndex = 1;
                        imgImageMain.ImageUrl = qrProduct.ProductMainImage;
                    }
                    else
                    {
                        mvImageMain.ActiveViewIndex = 0;
                    }
                    if (!string.IsNullOrEmpty(qrProduct.Product3DImage))
                    {
                        mvImage3D.ActiveViewIndex = 1;
                        imgImage3D.ImageUrl = qrProduct.Product3DImage;
                    }
                    else
                    {
                        mvImage3D.ActiveViewIndex = 0;
                    }


                    if (Request.QueryString["Mode"] == "Delete")
                    {

                        drpBrand.Enabled = false;
                        drpCat.Enabled = false;
                        drpSubCat.Enabled = false;
                        cblGenders.Enabled = false;
                        txtProductCode.Enabled = false;
                        txtProductName_FA.Enabled = false;
                        txtProductName_EN.Enabled = false;
                        txtProductDesc.Enabled = false;
                        fuImageMain.Visible = false;
                        fuImage3D.Visible = false;
                        lbImageMain.Visible = false;
                        lbImage3D.Visible = false;
                        btnSubmit.Visible = false;
                        btnDelete.Visible = true;


                    }
                    else
                    {


                        drpBrand.Enabled = true;
                        drpCat.Enabled = true;
                        drpSubCat.Enabled = true;
                        cblGenders.Enabled = true;
                        txtProductCode.Enabled = true;
                        txtProductName_FA.Enabled = true;
                        txtProductName_EN.Enabled = true;
                        txtProductDesc.Enabled = true;
                        fuImageMain.Visible = true;
                        fuImage3D.Visible = true;
                        lbImageMain.Visible = true;
                        lbImage3D.Visible = true;
                        btnSubmit.Visible = true;
                        btnDelete.Visible = false;

                    }






                }
                else
                {
                    hfProductID.Value = "No";
                }



            }






        }


        void FillGenders()
        {

            var qrGenders = (from gen in HisssAppDB.tblGenders
                             select gen).ToList();




            foreach (var GenderItem in qrGenders)
            {
                cblGenders.Items.Add(new ListItem(GenderItem.GenderTitle.ToString(), GenderItem.ID.ToString()));
              
            }


        }
        void FillGenders(int ProductID)
        {

            var qrGenders = (from gen in HisssAppDB.tblProducts_Genders
                             where gen.ProductID == ProductID
                             select gen).ToList();




            foreach (var GenderItem in qrGenders)
            {
                for (int i = 0; i < cblGenders.Items.Count ; i++)
                {
                    if (GenderItem.GenderID ==Convert.ToInt32(cblGenders.Items[i].Value))
                    {
                        cblGenders.Items[i].Selected = true;
                    }
                }

            }


        }


        void FillCategories()
        {

            var qrCats = (from Cats in HisssAppDB.tblProduct_Categories
                          select new ProductCategory()
                          {
                              ID = Cats.ID,
                              CategoryName_FA = Cats.CategoryName_FA
                          }).ToList();

            DataTable dtCats = new DataTable();
            dtCats.Columns.Add("Title");
            dtCats.Columns.Add("ID");

            foreach (var CatItem in qrCats)
            {
                dtCats.Rows.Add(CatItem.CategoryName_FA, CatItem.ID);
            }

            drpCat.DataSource = dtCats;
            drpCat.DataTextField = "Title";
            drpCat.DataValueField = "ID";
            drpCat.DataBind();
        }

        void FillSubCategories(int CatID)
        {

            var qrSubCats = (from SubCats in HisssAppDB.tblProduct_SubCategories
                          where SubCats.CategoryID == CatID
                          select new ProductSubCategory() 
                          {
                              ID = SubCats.ID,
                               SubCategoryName_FA= SubCats.SubCategoryName_FA
                          }).ToList();

            DataTable dtSubCats = new DataTable();
            dtSubCats.Columns.Add("Title");
            dtSubCats.Columns.Add("ID");

            foreach (var SubCatItem in qrSubCats)
            {
                dtSubCats.Rows.Add(SubCatItem.SubCategoryName_FA, SubCatItem.ID);
            }

            drpSubCat.DataSource = dtSubCats;
            drpSubCat.DataTextField = "Title";
            drpSubCat.DataValueField = "ID";
            drpSubCat.DataBind();
        }

        void FillBrands()
        {

            var qrBrands = (from BR in HisssAppDB.tblBrands
                            select new Brand()
                            {
                                ID = BR.ID,
                                BrandName_FA = BR.BrandName_FA
                            }).ToList();

            DataTable dtBrands = new DataTable();
            dtBrands.Columns.Add("Title");
            dtBrands.Columns.Add("ID");

            foreach (var BrandItem in qrBrands)
            {
                dtBrands.Rows.Add(BrandItem.BrandName_FA, BrandItem.ID);
            }

            drpBrand.DataSource = dtBrands;
            drpBrand.DataTextField = "Title";
            drpBrand.DataValueField = "ID";
            drpBrand.DataBind();
        }



        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            bool blnProductMainImage = false;
            bool blnProduct3DImage = false;

            if (hfProductID.Value != "No")
            {
                //edit
                var qrProductEdit = (from ST in HisssAppDB.tblProducts where ST.ID == Convert.ToInt32(hfProductID.Value.ToString()) select ST).First();


                qrProductEdit.SubCategoryID = Convert.ToInt32(drpSubCat.SelectedValue);
                qrProductEdit.BrandID = Convert.ToInt32(drpBrand.SelectedValue);
                qrProductEdit.ProductCode = txtProductCode.Text;
                qrProductEdit.ProductName_FA = txtProductName_FA.Text;
                qrProductEdit.ProductName_EN = txtProductName_EN.Text;
                qrProductEdit.ProductDesc = txtProductDesc.Text;

                InsertGenders(qrProductEdit.ID);

                if (fuImageMain.HasFiles)
                {

                    string strFileName = UtilityServices.GenerateFileName(fuImageMain.PostedFile.FileName);

                    qrProductEdit.ProductMainImage = "http://www.HisssApp.com/files/Products_Images/Main/" + strFileName;

                    blnProductMainImage = UtilityServices.SaveFile(Server.MapPath("~/Files/Products_Images/Main/"), fuImageMain,strFileName, ".jpg,.jpeg");

                }
                else
                {


                    if (qrProductEdit.ProductMainImage != null && qrProductEdit.ProductMainImage.ToString() != "")
                    {
                        blnProductMainImage = true;
                    }

                    else
                    {
                        blnProductMainImage = false;

                    }

                }
                if (fuImage3D.HasFiles)
                {

                    string strFileName = UtilityServices.GenerateFileName(fuImage3D.PostedFile.FileName);

                    qrProductEdit.Product3DImage = "http://api.gheseland.com/files/Products_Images/3D/" + strFileName;

                    blnProduct3DImage = UtilityServices.SaveFile(Server.MapPath("~/Files/Products_Images/3D/"), fuImage3D,strFileName, ".gif");
                }
                else
                {

                    if (qrProductEdit.Product3DImage != null && qrProductEdit.Product3DImage.ToString() != "")
                    {
                        blnProduct3DImage = true;
                    }

                    else
                    {
                        blnProduct3DImage = false;

                    }

                }



                if (blnProductMainImage)
                {
                    HisssAppDB.SubmitChanges();

                    lblMessage.Text = "اطلاعات با موفقیت ثبت شد .";
                    Response.Redirect("ManageProducts.aspx");
                }
                else
                {
                    lblMessage.Text = "فرمت فابلهای وارد شده مناسب نیست";
                }


            }
            else
            {
                //New 





                string strProductMainImage = "", strProduct3DImage = "";


                if (fuImageMain.HasFiles)
                {


                    string strFileName = UtilityServices.GenerateFileName(fuImageMain.PostedFile.FileName);

                    strProductMainImage = "http://www.HisssApp.com/files/Products_Images/Main/" + strFileName;

                    blnProductMainImage = UtilityServices.SaveFile(Server.MapPath("~/Files/Products_Images/Main/"), fuImageMain,strFileName, ".jpg,.jpeg");

                }

                if (fuImage3D.HasFiles)
                {

                    string strFileName = UtilityServices.GenerateFileName(fuImage3D.PostedFile.FileName);

                    strProduct3DImage = "http://api.gheseland.com/files/Products_Images/3D/" + strFileName;

                    blnProduct3DImage = UtilityServices.SaveFile(Server.MapPath("~/Files/Products_Images/3D/"), fuImage3D,strFileName, ".gif");
                }







                if (blnProductMainImage)
                {
                    tblProduct ProductObject = new tblProduct()
                   {
                       SubCategoryID = Convert.ToInt32(drpSubCat.SelectedValue),
                      BrandID = Convert.ToInt32(drpBrand.SelectedValue),
                       ProductCode = txtProductCode.Text,
                       ProductName_FA = txtProductName_FA.Text,
                       ProductName_EN = txtProductName_EN.Text,
                       ProductDesc = txtProductDesc.Text,
                       ProductMainImage = strProductMainImage,
                       Product3DImage = strProduct3DImage,
                       RegDate = DateTime.Now

                   };

                    HisssAppDB.tblProducts.InsertOnSubmit(ProductObject);
                    HisssAppDB.SubmitChanges();


                    InsertGenders(ProductObject.ID);





                    lblMessage.Text = "اطلاعات با موفقیت ثبت شد .";

                    Response.Redirect("ManageProducts.aspx");
                }

                else
                {
                    lblMessage.Text = "فرمت فابلهای وارد شده مناسب نیست";
                }

            }

        }



        void InsertGenders(int ProductID)
        {

            var qrDelete = (from del in HisssAppDB.tblProducts_Genders where del.ProductID == ProductID select del).ToList();


            if (qrDelete != null)
            {
                HisssAppDB.tblProducts_Genders.DeleteAllOnSubmit(qrDelete );

                HisssAppDB.SubmitChanges();
            }


            for (int i = 0; i < cblGenders.Items.Count  ; i++)
            {
                if (cblGenders.Items[i].Selected == true)
                {
                    tblProducts_Gender ProductGenObject = new tblProducts_Gender()
                    {
                        ProductID = ProductID,
                        GenderID =Convert.ToInt32(cblGenders.Items[i].Value)

                    };

                    HisssAppDB.tblProducts_Genders.InsertOnSubmit(ProductGenObject);
                }
            }

            HisssAppDB.SubmitChanges();


        }



        protected void lbImage3D_Click(object sender, EventArgs e)
        {


            try
            {

                string strImage;

                var qrDelete = (from BD in HisssAppDB.tblProducts where BD.ID == Convert.ToInt32(hfProductID.Value.ToString()) select BD).First();

                strImage = qrDelete.Product3DImage;

                strImage = "~" + strImage.Remove(0, 23);

                qrDelete.Product3DImage = "";

                HisssAppDB.SubmitChanges();

                UtilityServices.DeleteFile(Server.MapPath(strImage));


                mvImage3D.ActiveViewIndex = 0;
                imgImage3D.ImageUrl = "";

            }
            catch (Exception ex)
            {

                lblMessage.Text = ex.Message;

            }




        }

        protected void lbImageMain_Click(object sender, EventArgs e)
        {
            try
            {

                string strImage;

                var qrDelete = (from BD in HisssAppDB.tblProducts where BD.ID == Convert.ToInt32(hfProductID.Value.ToString()) select BD).First();

                strImage = qrDelete.ProductMainImage;

                strImage = "~" + strImage.Remove(0, 23);

                qrDelete.ProductMainImage = "";

                HisssAppDB.SubmitChanges();

                UtilityServices.DeleteFile(Server.MapPath(strImage));


                mvImageMain.ActiveViewIndex = 0;
                imgImageMain.ImageUrl = "";

            }
            catch (Exception ex)
            {

                lblMessage.Text = ex.Message;

            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {





            try
            {

                string strImageMain, strImage3D;

                var qrDelete = (from PD in HisssAppDB.tblProducts where PD.ID == Convert.ToInt32(hfProductID.Value.ToString()) select PD).First();

                






                HisssAppDB.tblProducts.DeleteOnSubmit(qrDelete);

                HisssAppDB.SubmitChanges();



                strImageMain = qrDelete.ProductMainImage;

                strImageMain = "~" + strImageMain.Remove(0, 23);



                if (qrDelete.Product3DImage != null && qrDelete.Product3DImage != "")
                {
                    strImage3D = qrDelete.Product3DImage;

                    strImage3D = "~" + strImage3D.Remove(0, 23);

                    UtilityServices.DeleteFile(Server.MapPath(strImage3D));


                }


                UtilityServices.DeleteFile(Server.MapPath(strImageMain));


         




                Response.Redirect("ManageProducts.aspx");

            }
            catch (Exception ex)
            {

                lblMessage.Text = ex.Message;

            }










        }

        protected void drpCat_SelectedIndexChanged(object sender, EventArgs e)
        {



            FillSubCategories(Convert.ToInt32(drpCat.SelectedValue));





        }






    }
}