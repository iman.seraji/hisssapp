﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="AddEditProductsCategory.aspx.cs" Inherits="HisssApp.Admin.AddEditProductsCategory" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SiteMap" runat="server">


    <li><a href="AdminDashboard.aspx"><i class="fa fa-dashboard"></i>داشبرد</a></li>

    <li><a href="ManageProductsCategories.aspx">مدیریت گروه اصلی محصولات</a></li>
    <li class="active">ویرایش گروه اصلی محصولات</li>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    ویرایش گروه اصلی محصولات
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">

    <table>
        <tr>
            <td colspan="2">


                <asp:Label ID="lblDelete" runat="server" ForeColor="Red" Visible="false" Text="کاربر گرامی درصورتی که برای حذف مطمئن هستید دگمه حذف را بزنید .!!!!!"></asp:Label>
                <br />
                <br />
                <br />
                <br />


            </td>
        </tr>
        <tr>
            <td>نام فارسی : </td>
            <td>
                <asp:TextBox ID="txtCategoryName_FA" runat="server" Width="630px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="لطفا نام فارسی را وارد نمایید ." ControlToValidate="txtCategoryName_FA"></asp:RequiredFieldValidator>

            </td>
        </tr>
        <tr>
            <td>نام انگلیسی : </td>
            <td>
                <asp:TextBox ID="txtCategoryName_EN" runat="server" Width="630px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="لطفا نام انگلیسی را وارد نمایید ." ControlToValidate="txtCategoryName_EN"></asp:RequiredFieldValidator>

            </td>
        </tr>


        <tr>
            <td colspan="2">

                <br />
                <br />
                <hr/>
                <asp:Button ID="btnDelete" runat="server" Text="حذف" Width="90px" Visible="false" ValidationGroup="Delete" class="btn btn-info" OnClick="btnDelete_Click" />
                <asp:Button ID="btnSubmit" runat="server" Text="ثبت" Width="90px" class="btn btn-success" OnClick="btnSubmit_Click" />
                <asp:Button ID="Button1" runat="server" Text="انصراف" Width="90px" PostBackUrl="ManageProductsCategories.aspx" ValidationGroup="Cancel" class="btn btn-danger" />

            </td>
        </tr>
        <tr>
            <td colspan="2">

                <asp:Label ID="lblMessage" runat="server" ForeColor="red" Text=""></asp:Label>

                <asp:HiddenField ID="hfCategoryID" runat="server" />

            </td>
        </tr>
    </table>

</asp:Content>



