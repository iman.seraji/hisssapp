﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using HisssApp.Services;

namespace HisssApp.Admin
{
    public partial class AddEditProductsCategory : System.Web.UI.Page
    {
        HisssAppDBDataContext HisssAppDB = new HisssAppDBDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {



                if (Request.QueryString["PCID"] != null)
                {



                    hfCategoryID.Value = Request.QueryString["PCID"].ToString();
                    int intBrandID = Convert.ToInt32(Request.QueryString["PCID"]);

                    var qrCategory = (from BR in HisssAppDB.tblProduct_Categories where BR.ID == intBrandID select BR).First();



                    txtCategoryName_FA.Text = qrCategory.CategoryName_FA;
                    txtCategoryName_EN.Text = qrCategory.CategoryName_EN;







                    if (Request.QueryString["Mode"] == "Delete")
                    {

                        txtCategoryName_FA.Enabled = false;
                        txtCategoryName_EN.Enabled = false;
                        lblDelete.Visible = true;
                        btnDelete.Visible = true;
                        btnSubmit.Visible = false;


                    }
                    else
                    {
                        txtCategoryName_FA.Enabled = true;
                        txtCategoryName_EN.Enabled = true;

                        lblDelete.Visible = false;
                        btnDelete.Visible = false;
                        btnSubmit.Visible = true;
                    }






                }
                else
                {
                    hfCategoryID.Value = "No";
                }



            }



        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {




            bool blnBrandImage = false;






            if (hfCategoryID.Value != "No")
            {

                var qrCategoryEdit = (from BE in HisssAppDB.tblProduct_Categories where BE.ID == Convert.ToInt32(hfCategoryID.Value.ToString()) select BE).First();

                qrCategoryEdit.CategoryName_FA = txtCategoryName_FA.Text;
                qrCategoryEdit.CategoryName_EN = txtCategoryName_EN.Text;



                HisssAppDB.SubmitChanges();

                lblMessage.Text = "اطلاعات با موفقیت ثبت شد .";
                Response.Redirect("ManageProductsCategories.aspx");



            }
            else
            {



                tblProduct_Category CategoryObject = new tblProduct_Category()
                    {

                        CategoryName_FA = txtCategoryName_FA.Text,
                        CategoryName_EN = txtCategoryName_EN.Text,
                        

                    };

                HisssAppDB.tblProduct_Categories.InsertOnSubmit(CategoryObject);
                    HisssAppDB.SubmitChanges();
                    lblMessage.Text = "اطلاعات با موفقیت ثبت شد .";

                    Response.Redirect("ManageProductsCategories.aspx");
              







            }





        }

     

        protected void btnDelete_Click(object sender, EventArgs e)
        {



            try
            {

                 
                var qrDelete = (from BD in HisssAppDB.tblProduct_Categories where BD.ID == Convert.ToInt32(hfCategoryID.Value.ToString()) select BD).First();

         
                HisssAppDB.tblProduct_Categories.DeleteOnSubmit(qrDelete);

                HisssAppDB.SubmitChanges();




                Response.Redirect("ManageProductsCategories.aspx");

            }
            catch (Exception ex)
            {

                lblMessage.Text = ex.Message;

            }




        }
    }
}