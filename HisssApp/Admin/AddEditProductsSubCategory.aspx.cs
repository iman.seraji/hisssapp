﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Models;
using DAL;

namespace HisssApp.Admin
{
    public partial class AddEditProductsSubCategory : System.Web.UI.Page
    {
        HisssAppDBDataContext HisssAppDB = new HisssAppDBDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {

                FillCategories();

                if (Request.QueryString["PSCID"] != null)
                {



                    hfSubCategoryID.Value = Request.QueryString["PSCID"].ToString();
                    int intSubCategoryID = Convert.ToInt32(Request.QueryString["PSCID"]);

                    var qrSubCategory = (from BR in HisssAppDB.tblProduct_SubCategories where BR.ID == intSubCategoryID select BR).First();



                    txtSubCategoryName_FA.Text = qrSubCategory.SubCategoryName_FA;
                    txtSubCategoryName_EN.Text = qrSubCategory.SubCategoryName_EN;







                    if (Request.QueryString["Mode"] == "Delete")
                    {

                        txtSubCategoryName_FA.Enabled = false;
                        txtSubCategoryName_EN.Enabled = false;
                        drpCat.Enabled = false;
                        lblDelete.Visible = true;
                        btnDelete.Visible = true;
                        btnSubmit.Visible = false;


                    }
                    else
                    {
                        txtSubCategoryName_FA.Enabled = true;
                        txtSubCategoryName_EN.Enabled = true;
                        drpCat.Enabled = true;
                        lblDelete.Visible = false;
                        btnDelete.Visible = false;
                        btnSubmit.Visible = true;
                    }






                }
                else
                {
                    hfSubCategoryID.Value = "No";
                }



            }



        }


        void FillCategories()
        {

            var qrCats = (from Cats in HisssAppDB.tblProduct_Categories
                          select new ProductCategory()
                          {
                              ID = Cats.ID,
                              CategoryName_FA = Cats.CategoryName_FA
                          }).ToList();

            DataTable dtCats = new DataTable();
            dtCats.Columns.Add("Title");
            dtCats.Columns.Add("ID");

            foreach (var CatItem in qrCats)
            {
                dtCats.Rows.Add(CatItem.CategoryName_FA, CatItem.ID);
            }

            drpCat.DataSource = dtCats;
            drpCat.DataTextField = "Title";
            drpCat.DataValueField = "ID";
            drpCat.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {




            bool blnBrandImage = false;






            if (hfSubCategoryID.Value != "No")
            {

                var qrSubCategoryEdit = (from BE in HisssAppDB.tblProduct_SubCategories where BE.ID == Convert.ToInt32(hfSubCategoryID.Value.ToString()) select BE).First();

                qrSubCategoryEdit.CategoryID = Convert.ToInt32(drpCat.SelectedValue);
                qrSubCategoryEdit.SubCategoryName_FA = txtSubCategoryName_FA.Text;
                qrSubCategoryEdit.SubCategoryName_EN = txtSubCategoryName_EN.Text;



                HisssAppDB.SubmitChanges();

                lblMessage.Text = "اطلاعات با موفقیت ثبت شد .";
                Response.Redirect("ManageProductsSubCategories.aspx");



            }
            else
            {



                tblProduct_SubCategory SubCategoryObject = new tblProduct_SubCategory()
                {
                    CategoryID =  Convert.ToInt32(drpCat.SelectedValue),
                    SubCategoryName_FA = txtSubCategoryName_FA.Text,
                    SubCategoryName_EN = txtSubCategoryName_EN.Text,


                };

                HisssAppDB.tblProduct_SubCategories.InsertOnSubmit(SubCategoryObject);
                HisssAppDB.SubmitChanges();
                lblMessage.Text = "اطلاعات با موفقیت ثبت شد .";

                Response.Redirect("ManageProductsSubCategories.aspx");








            }





        }



        protected void btnDelete_Click(object sender, EventArgs e)
        {



            try
            {


                var qrDelete = (from BD in HisssAppDB.tblProduct_SubCategories where BD.ID == Convert.ToInt32(hfSubCategoryID.Value.ToString()) select BD).First();


                HisssAppDB.tblProduct_SubCategories.DeleteOnSubmit(qrDelete);

                HisssAppDB.SubmitChanges();




                Response.Redirect("ManageProductsSubCategories.aspx");

            }
            catch (Exception ex)
            {

                lblMessage.Text = ex.Message;

            }




        }
    }
}