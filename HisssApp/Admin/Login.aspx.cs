﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DAL;

namespace HisssApp.Admin
{
    public partial class Login : System.Web.UI.Page
    {
        HisssAppDBDataContext HisssAppDB = new HisssAppDBDataContext();
       


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                txtPassword.Value = "";

            }


        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {

            var adminInfo =
                (from AI in HisssAppDB.tblAdmins
                 where AI.Username == txtUser.Value && AI.Password == txtPassword.Value && AI.IsActive == 1
                 select AI).FirstOrDefault();


            if (adminInfo != null)
            {


                if (adminInfo.Type == 1)
                {
                    HttpContext.Current.Session["AdminInfo"] = string.Format("{0}-{1}-{2}", adminInfo.ID.ToString(), adminInfo.Username.ToString(), adminInfo.Type.ToString());
                    Response.Redirect("AdminDashboard.aspx");
                }
                else
                {
                    HttpContext.Current.Session["AdminInfo"] = string.Format("{0}-{1}-{2}", adminInfo.ID.ToString(), adminInfo.Username.ToString(), adminInfo.Type.ToString());
                    Response.Redirect("OpratorDashboard.aspx");
                }




            }
            else
            {
                lblMessage.Text = "نام کاربری یا کلمه عبور صحیح نیست .";
            }











        }
    }
}