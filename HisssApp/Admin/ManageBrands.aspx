﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="ManageBrands.aspx.cs" Inherits="HisssApp.Admin.ManageBrands" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SiteMap" runat="server">


    <li><a href="AdminDashboard.aspx"><i class="fa fa-dashboard"></i>داشبرد</a></li>

    <li class="active">مدیریت برند ها</li>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    مدیریت برند ها
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">


    <a href='AddEditBrands.aspx' class="btn btn-app">
        <i class="fa fa-save"></i>
        برند جدید
    </a>

    <asp:GridView ID="grvBrands" runat="server" AutoGenerateColumns="False" GridLines="None" class="table table-bordered table-hover" AllowSorting="True" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:TemplateField HeaderText="تصویر" ItemStyle-Width="50px" SortExpression="BrandImage">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("BrandImage")%>'  Width="115 PX " Height="60 px"/>
                </ItemTemplate>
                <ItemStyle Width="150px"></ItemStyle>
            </asp:TemplateField>
            <asp:BoundField DataField="BrandName_FA" HeaderText="نام فارسی" SortExpression="BrandName_FA" />
            <asp:BoundField DataField="BrandName_EN" HeaderText="نام انگلیسی" SortExpression="BrandName_EN" />

            <asp:TemplateField HeaderText="" ItemStyle-Width="100px">
                <ItemTemplate>
                    <a href='AddEditBrands.aspx?BID=<%# Eval("ID")%>'>ویرایش</a>
                </ItemTemplate>
                <ItemStyle Width="100px"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="" ItemStyle-Width="100px">
                <ItemTemplate>
                    <a href='AddEditBrands.aspx?Mode=Delete&BID=<%# Eval("ID")%>'>حذف</a>
                </ItemTemplate>
                <ItemStyle Width="100px"></ItemStyle>
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" VerticalAlign="Middle" HorizontalAlign="Center" />
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" VerticalAlign="Middle" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <SortedAscendingCellStyle BackColor="#FDF5AC" />
        <SortedAscendingHeaderStyle BackColor="#4D0000" />
        <SortedDescendingCellStyle BackColor="#FCF6C0" />
        <SortedDescendingHeaderStyle BackColor="#820000" />
    </asp:GridView>


</asp:Content>
