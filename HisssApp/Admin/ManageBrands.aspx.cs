﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Models;

namespace HisssApp.Admin
{
    public partial class ManageBrands : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {



            HisssAppDBDataContext HisssAppDB = new HisssAppDBDataContext();


            var Brands = (from BN in HisssAppDB.tblBrands
                           orderby BN.BrandPriority 

                           select new Brand()
                           {
                               ID = BN.ID,
                               BrandName_FA = BN.BrandName_FA,
                               BrandName_EN = BN.BrandName_EN,
                               BrandImage = BN.BrandImage,
                               BrandIsAvtive = BN.BrandIsActive,
                               BrandPriority = BN.BrandPriority ?? 0


                           }).ToList();

            grvBrands.DataSource = Brands;
            grvBrands.DataBind();







        }
    }
}