﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="ManageProducts.aspx.cs" Inherits="HisssApp.Admin.ManageProducts" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SiteMap" runat="server">


    <li><a href="AdminDashboard.aspx"><i class="fa fa-dashboard"></i>داشبرد</a></li>

    <li class="active">مدیریت محصولات</li>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    مدیریت محصولات
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">





    <div class="col-md-1">
        <a href='AddEditProducts.aspx' class="btn btn-app">
            <i class="fa fa-save"></i>
            محصول جدید
    </a>
    </div>

   <%-- <div class="col-md-2">
        گروه اصلی : 
        <asp:DropDownList ID="drpCategories" runat="server" Width="200px" AutoPostBack="True" OnSelectedIndexChanged="drpCategories_SelectedIndexChanged">
            <asp:ListItem Selected="True" Value="0">همه</asp:ListItem>

        </asp:DropDownList>


    </div>

    <div class="col-md-2">
        زیر گروه :
        <asp:DropDownList ID="drpSubCategories" runat="server" Width="200px" AutoPostBack="True" OnSelectedIndexChanged="drpCategories_SelectedIndexChanged">
            <asp:ListItem Selected="True" Value="0">همه</asp:ListItem>

        </asp:DropDownList>


    </div>

    <div class="col-md-2">
      برند ها :
        <asp:DropDownList ID="drpBrands" runat="server" Width="200px" AutoPostBack="True" OnSelectedIndexChanged="drpCategories_SelectedIndexChanged">
            <asp:ListItem Selected="True" Value="0">همه</asp:ListItem>

        </asp:DropDownList>


    </div>
    
        <div class="col-md-2">
            جنسیت : 
        <asp:DropDownList ID="drpGenders" runat="server" Width="200px" AutoPostBack="True" OnSelectedIndexChanged="drpCategories_SelectedIndexChanged">
            <asp:ListItem Selected="True" Value="0">همه</asp:ListItem>

        </asp:DropDownList>


    </div>--%>

    <asp:GridView ID="grvProduts" runat="server" AutoGenerateColumns="False" GridLines="None" class="table table-bordered table-hover" AllowSorting="True" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center">
        <AlternatingRowStyle BackColor="White" />
        <Columns>

            <asp:TemplateField HeaderText="تصویر" ItemStyle-Width="50px" SortExpression="BrandImage">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("ProductMainImage")%>' Width="50 PX " Height="50 px" />
                </ItemTemplate>
                <ItemStyle Width="150px"></ItemStyle>
            </asp:TemplateField>

            <asp:BoundField DataField="BrandName_FA" HeaderText="برند" SortExpression="BrandName_FA">
                <ItemStyle Width="200px" />
            </asp:BoundField>
            <asp:BoundField DataField="GenderTitle" HeaderText="جنسیت" SortExpression="GenderTitle">
                <ItemStyle Width="200px" />
            </asp:BoundField>

            <asp:BoundField DataField="CategoryName_FA" HeaderText="گروه اصلی" SortExpression="CategoryName_FA">
                <ItemStyle Width="200px" />
            </asp:BoundField>
            <asp:BoundField DataField="SubCategoryName_FA" HeaderText="زیر گروه" SortExpression="SubCategoryName_FA">
                <ItemStyle Width="200px" />
            </asp:BoundField>
            <asp:BoundField DataField="ProductCode" HeaderText="کد محصول" SortExpression="ProductCode" />
            <asp:BoundField DataField="ProductName_FA" HeaderText="نام محصول" SortExpression="ProductName_FA" />


            <asp:TemplateField HeaderText="" ItemStyle-Width="100px">
                <ItemTemplate>
                    <a href='AddEditProducts.aspx?PID=<%# Eval("ID")%>'>ویرایش</a>
                </ItemTemplate>
                <ItemStyle Width="100px"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="" ItemStyle-Width="100px">
                <ItemTemplate>
                    <a href='AddEditProductPrices.aspx?PID=<%# Eval("ID")%>'>ثبت قیمت</a>
                </ItemTemplate>
                <ItemStyle Width="100px"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="" ItemStyle-Width="100px">
                <ItemTemplate>
                    <a href='AddEditProducts.aspx?Mode=Delete&PID=<%# Eval("ID")%>'>حذف</a>
                </ItemTemplate>
                <ItemStyle Width="100px"></ItemStyle>
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" VerticalAlign="Middle" HorizontalAlign="Center" />
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" VerticalAlign="Middle" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <SortedAscendingCellStyle BackColor="#FDF5AC" />
        <SortedAscendingHeaderStyle BackColor="#4D0000" />
        <SortedDescendingCellStyle BackColor="#FCF6C0" />
        <SortedDescendingHeaderStyle BackColor="#820000" />
    </asp:GridView>


</asp:Content>
