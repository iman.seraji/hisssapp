﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Models;

namespace HisssApp.Admin
{
    public partial class ManageProducts : System.Web.UI.Page
    {

        HisssAppDBDataContext HisssAppDB = new HisssAppDBDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                //FillGenders();
                //FillBrands();
                //FillCategories();
                //FillSubCategories(Convert.ToInt32(drpCategories.SelectedValue));

                var Products = (from PD in HisssAppDB.vwProducts
                    orderby PD.RegDate descending
                    select PD).ToList();

                grvProduts.DataSource = Products;
                grvProduts.DataBind();

            }

        }


        //void FillGenders()
        //{

        //    var qrGenders = (from gen in HisssAppDB.tblGenders
        //                     select gen).ToList();


        //    DataTable dtGenders = new DataTable();
        //    dtGenders.Columns.Add("Title");
        //    dtGenders.Columns.Add("ID");

        //    foreach (var GenderItem in qrGenders)
        //    {
        //        dtGenders.Rows.Add(GenderItem.GenderTitle, GenderItem.ID);
        //    }

        //    drpGenders.DataSource = dtGenders;
        //    drpGenders.DataTextField = "Title";
        //    drpGenders.DataValueField = "ID";
        //    drpGenders.DataBind();
        //    drpGenders.Items.Insert(0, new ListItem("همه", "0"));


        //}



        //void FillCategories()
        //{

        //    var qrCats = (from Cats in HisssAppDB.tblProduct_Categories
        //                  select new ProductCategory()
        //                  {
        //                      ID = Cats.ID,
        //                      CategoryName_FA = Cats.CategoryName_FA
        //                  }).ToList();

        //    DataTable dtCats = new DataTable();
        //    dtCats.Columns.Add("Title");
        //    dtCats.Columns.Add("ID");

        //    foreach (var CatItem in qrCats)
        //    {
        //        dtCats.Rows.Add(CatItem.CategoryName_FA, CatItem.ID);
        //    }

        //    drpCategories.DataSource = dtCats;
        //    drpCategories.DataTextField = "Title";
        //    drpCategories.DataValueField = "ID";
        //    drpCategories.DataBind();
        //    drpCategories.Items.Insert(0, new ListItem("همه", "0"));
        //}

        //void FillSubCategories(int CatID)
        //{

        //    var qrSubCats = (from SubCats in HisssAppDB.tblProduct_SubCategories
        //                     where SubCats.CategoryID == CatID
        //                     select new ProductSubCategory()
        //                     {
        //                         ID = SubCats.ID,
        //                         SubCategoryName_FA = SubCats.SubCategoryName_FA
        //                     }).ToList();

        //    if (CatID == 0)
        //    {
        //         qrSubCats = (from SubCats in HisssAppDB.tblProduct_SubCategories
        //                         select new ProductSubCategory()
        //                         {
        //                             ID = SubCats.ID,
        //                             SubCategoryName_FA = SubCats.SubCategoryName_FA
        //                         }).ToList();

        //    }

        //    DataTable dtSubCats = new DataTable();
        //    dtSubCats.Columns.Add("Title");
        //    dtSubCats.Columns.Add("ID");

        //    foreach (var SubCatItem in qrSubCats)
        //    {
        //        dtSubCats.Rows.Add(SubCatItem.SubCategoryName_FA, SubCatItem.ID);
        //    }

        //    drpSubCategories.DataSource = dtSubCats;
        //    drpSubCategories.DataTextField = "Title";
        //    drpSubCategories.DataValueField = "ID";
        //    drpSubCategories.DataBind();
        //    drpSubCategories.Items.Insert(0, new ListItem("همه", "0"));
        //}

        //void FillBrands()
        //{

        //    var qrBrands = (from BR in HisssAppDB.tblBrands
        //                    select new Brand()
        //                    {
        //                        ID = BR.ID,
        //                        BrandName_FA = BR.BrandName_FA
        //                    }).ToList();

        //    DataTable dtBrands = new DataTable();
        //    dtBrands.Columns.Add("Title");
        //    dtBrands.Columns.Add("ID");

        //    foreach (var BrandItem in qrBrands)
        //    {
        //        dtBrands.Rows.Add(BrandItem.BrandName_FA, BrandItem.ID);
        //    }

        //    drpBrands.DataSource = dtBrands;
        //    drpBrands.DataTextField = "Title";
        //    drpBrands.DataValueField = "ID";
        //    drpBrands.DataBind();
        //    drpBrands.Items.Insert(0, new ListItem("همه", "0"));
        //}

        //protected void drpCategories_SelectedIndexChanged(object sender, EventArgs e)
        //{



            

        //    var Products = (from PD in HisssAppDB.vwProducts
        //                    orderby PD.RegDate descending
        //                    select PD).ToList();

        //    grvProduts.DataSource = Products;
        //    grvProduts.DataBind();



        //}




    }
}