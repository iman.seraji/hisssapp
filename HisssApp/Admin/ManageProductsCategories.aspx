﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="ManageProductsCategories.aspx.cs" Inherits="HisssApp.Admin.ManageProductsCategories" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SiteMap" runat="server">


    <li><a href="AdminDashboard.aspx"><i class="fa fa-dashboard"></i>داشبرد</a></li>

    <li class="active">  مدیریت گروه اصلی محصولات</li>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    مدیریت گروه اصلی محصولات
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">


    <a href='AddEditProductsCategory.aspx' class="btn btn-app">
        <i class="fa fa-save"></i>
        گروه اصلی  جدید
    </a>

    <asp:GridView ID="grvProductsCategories" runat="server" AutoGenerateColumns="False" GridLines="None" class="table table-bordered table-hover" AllowSorting="True" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
           
            <asp:BoundField DataField="CategoryName_FA" HeaderText="نام فارسی" SortExpression="CategoryName_FA" />
            <asp:BoundField DataField="CategoryName_EN" HeaderText="نام انگلیسی" SortExpression="CategoryName_EN" />

            <asp:TemplateField HeaderText="" ItemStyle-Width="100px">
                <ItemTemplate>
                    <a href='AddEditProductsCategory.aspx?PCID=<%# Eval("ID")%>'>ویرایش</a>
                </ItemTemplate>
                <ItemStyle Width="100px"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="" ItemStyle-Width="100px">
                <ItemTemplate>
                    <a href='AddEditProductsCategory.aspx?Mode=Delete&PCID=<%# Eval("ID")%>'>حذف</a>
                </ItemTemplate>
                <ItemStyle Width="100px"></ItemStyle>
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" VerticalAlign="Middle" HorizontalAlign="Center" />
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" VerticalAlign="Middle" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <SortedAscendingCellStyle BackColor="#FDF5AC" />
        <SortedAscendingHeaderStyle BackColor="#4D0000" />
        <SortedDescendingCellStyle BackColor="#FCF6C0" />
        <SortedDescendingHeaderStyle BackColor="#820000" />
    </asp:GridView>


</asp:Content>
