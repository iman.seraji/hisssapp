﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL.Models;

namespace HisssApp.Admin
{
    public partial class ManageProductsCategories : System.Web.UI.Page
    {
        HisssAppDBDataContext HisssAppDB = new HisssAppDBDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {


            var ProductsCategories = (from PC in HisssAppDB.tblProduct_Categories orderby PC.RegDate descending select  PC).ToList();

            grvProductsCategories.DataSource = ProductsCategories;
            grvProductsCategories.DataBind();







        }
    }
}