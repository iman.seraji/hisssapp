﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;

namespace HisssApp.Admin
{
    public partial class ManageProductsSubCategories : System.Web.UI.Page
    {
        HisssAppDBDataContext HisssAppDB = new HisssAppDBDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

            FillSubCats();
            if (!IsPostBack)
            {


                fillCats();
               

            }
           


           

        }


        void FillSubCats()
        {


            var ProductsSubCategories = (from PC in HisssAppDB.wvSubCategories orderby PC.RegDate descending select PC).ToList();

            if(drpCategories.SelectedValue != "0" )
            {


                ProductsSubCategories = (from PC in HisssAppDB.wvSubCategories where PC.CategoryID == Convert.ToInt32(drpCategories.SelectedValue) orderby PC.RegDate descending select PC).ToList();


            }



            grvProductsSubCategories.DataSource = ProductsSubCategories;
            grvProductsSubCategories.DataBind();


        }
         void fillCats()
        {


            var qrCats = (from Cats in HisssAppDB.tblProduct_Categories
                          select Cats).ToList();

            drpCategories.DataSource = qrCats;
            drpCategories.DataTextField = "CategoryName_FA";
            drpCategories.DataValueField = "ID";
            drpCategories.DataBind();
            drpCategories.Items.Insert(0, new ListItem("همه", "0"));




        }

         protected void drpCategories_SelectedIndexChanged(object sender, EventArgs e)
         {

         }

   
    }
}