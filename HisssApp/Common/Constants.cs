﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HisssApp.Common
{
    public class Constants
    {

        public class EnrollmentPaymentType
        {
            public const int Credit = 1;
            public const int Cash = 2;
            public const int GiftRateOne = 3;
            public const int GiftAfterSale = 4;
            //public const int GiftWithPrice = 5;
        }
        public class GiftPublicProductLimitType
        {
            public const int ProductWithNoLimit = 1;
            public const int ClubWithSportLimit = 2;
            public const int ClubWithClassLimit = 3;
        }

        public class AccountingType
        {
            public const int Deposit = 1;
            public const int Deduction = 2;
            public const int Bonus = 4;
            public const int GiftCard = 5;
            public const int CancelPurchase = 8;
            public const int PurchaseReturnment = 9;
            public const int IncreaseCredit = 10;
        }


        public class ProductType
        {
            public const int MembershipPakage = 1;
            public const int OneMonthFree = 2;
            public const int OneDayFree = 3;
            public const int EventGift = 4;

        }
        public class PaymentType
        {
            public const int Credit = 1;
            public const int Cash = 2;
            public const int GiftRateOne = 3;
            public const int GiftAfterSale = 4;
        }

        public class BankGateway
        {
            public const int Pasargad = 1;
            public const int Bazzar = 2;
            public const int Free = 3;
            public const int ParsianMPL = 4;
            public const int All = 5;

        }


        public class RequestType
        {
            public const int AndroidBazzar = 1;
            public const int AndroidDirect = 2;
            public const int IosDirect = 3;
            public const int Gift = 4;

        }

        public class Setting
        {
            public const string Version = "Ver";
        }

        public class EmailSubject
        {
            public const string UserRegistration = "ثبت نام در هیس اپ";
            public const string Winners = "اسامی برنده شدگان قرعه کشی هیس اپ";
            public const string Newsletter = "خبرنامه - هیس اپ";
            public const string ForgetPassword = "هیس اپ - بازیابی رمز عبور";
            public const string LogMonitor = "خطا در هیس اپ";
            public const string VoucherList = "کوپن های خریداری شده - هیس اپ";
            public const string ErroreToAdmin = "خطایی در سایت هیس اپ رخ داده";
            public const string PaymentNotification = "خرید از هیس اپ";
            public const string RegisterCode = "کد ورود به اپلیکیشن قصه گویی هیس اپ";
        }


        public class TemplateFiles
        {
            public const string UserEmailForgetPass = "UserEmailForgetPass.htm";
            public const string UserEmailRegistration = "UserEmailRegistration.htm";
            public const string GeneralEmail = "GeneralEmail.htm";
            public const string UnActivedUsers = "UnActivedUsers.html";
            public const string EmailCoachRegister = "EmailCoachRegister.html";
            public const string EmailLogMonitor = "EmailLogMonitor.htm";
            public const string Newsletter = "Newsletter.html";
            public const string Winners = "Winners.html";
            public const string VoucherList = "VoucherList.html";
            public const string UserDietVote = "EmailVoteDiet.html";
            public const string UserErrorReport = "UserErrorReport.html";
            public const string RegisterCode = "RegisterCode.html";

            public const string TemplateDirectory = "/Templates";
        }

        public class ImageSize
        {
            public const string Thumbnail = "ThumbNail/";
            public const string Medium = "Medium/";
            public const string Original = "Original/";
        }





        public class LogType
        {
            public const int Insert = 1;
            public const int Update = 2;
            public const int Delete = 3;
        }



        public class TransactionStatus
        {
            public const int NotPaid = 1;
            public const int Paid = 2;
            public const int BackMoney = 3;
        }

        public class RequestStatus
        {
            public const int GoingBank = 1;
            public const int NotGoingPaid = 2;
        }
        public class RequestVoucherStatus
        {
            public const int Used = 2;
            public const int NotUsed = 1;
            public const int Cleared = 3;
        }

        public class ActionStatus
        {
            public const int Insert = 1;
            public const int Delete = 2;
        }

        public class Payment
        {
            public const int Discount = 0;
        }

        public class Gender
        {
            public const char Male = 'M';
            public const char Female = 'F';
            public const char Both = 'B';
            public const char Unknown = 'U';
        }




        /// <summary>
        /// کلاس ارتباطات ایمیل/اس-ام-اس
        /// </summary>
        public class Communication
        {
            /// <summary>
            /// ثابت-های اس-ام-اس
            /// </summary>
            public class SMS
            {
                public const string GiftRecive = "دوست عزیز! خرید شما با موفقیت انجام شد. برای مشاهده هدیه هیس اپ خود به پنل کاربری تان مراجعه نمایید. هیس اپ";
                public const string CoachRegisterComplitedWaitingForAccept = "مربی عزیز به هیس اپ خوش آمدید اطلاعات شما به مدیریت ارسال شد و پس از تایید اطلاعات پیامک شروع به کار برای شما ارسال خواهد شد.";
            }

            /// <summary>
            /// ثابت-های ایمیل
            /// </summary>
            public class Email
            {

            }
        }

    }
}