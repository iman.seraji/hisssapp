﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;

namespace HisssApp.Common
{
    public class EmailStructure
    {
        private string _bodyFileName;

        public static string From
        {
            get { return "support@gheseland.com"; }
        }
        public string Subject { get; private set; }
        public List<string> To { get; private set; }
        public string Body { get; set; }
        public string DisplayName { get; set; }
        public string BodyFileName
        {
            get { return _bodyFileName; }
            set
            {
                if (string.IsNullOrEmpty(value) == false &&
                    _bodyFileName != value)
                {
                    _bodyFileName = value;
                    string fullPath = HttpContext.Current.Server.MapPath(Constants.TemplateFiles.TemplateDirectory + Path.DirectorySeparatorChar + _bodyFileName);
                    Body = File.ReadAllText(fullPath, Encoding.UTF8);
                }
            }
        }
        /// <summary>
        /// ارسال ایمیل گروهی
        /// </summary>
        /// <param name="subject">موضوع ایمیل</param>
        /// <param name="toList">لیستی از ایمیل های مورد نظر </param>
        public EmailStructure(string subject, List<string> toList)
        {
            Subject = subject;
            To = toList;
        }

        /// <summary>
        /// ارسال ایمیل تکی
        /// </summary>
        /// <param name="subject">موضوع ایمیل</param>
        /// <param name="to">ایمیل مورد نظر</param>
        public EmailStructure(string subject, string to)
            : this(subject, new List<string>() { to }) { }
    }
}