﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL;
using System.IO;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using HisssApp.Common;
using HisssApp.Services;
using BLL.Models;
using System.Text.RegularExpressions;

namespace HisssApp.Common
{
    public class GeneralClass
    {
        HisssAppDBDataContext HisssAppDB = new HisssAppDBDataContext();


        public static class Security
        {


            public static class Encryptor
            {

                public static string SafeUrlEncoder(string input)
                {
                    input = input.Replace("=", "%D8%AF");
                    input = input.Replace("+", "%D8%A8");
                    input = input.Replace("/", "%DB%8C");
                    return input;
                }
                public static string SafeUrlDecoder(string input)
                {
                    input = input.Replace("د", "=");
                    input = input.Replace("ب", "+");
                    input = input.Replace("ی", "/");
                    return input;
                }

                // This constant is used to determine the keysize of the encryption algorithm in bits.
                // We divide this by 8 within the code below to get the equivalent number of bytes.
                private const int Keysize = 256;

                // This constant determines the number of iterations for the password bytes generation function.
                private const int DerivationIterations = 1000;

                public static string EncryptUrlFriend(string plainText, string passPhrase)
                {
                    // Salt and IV is randomly generated each time, but is preprended to encrypted cipher text
                    // so that the same Salt and IV values can be used when decrypting.  
                    var saltStringBytes = Generate256BitsOfRandomEntropy();
                    var ivStringBytes = Generate256BitsOfRandomEntropy();
                    var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
                    using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
                    {
                        var keyBytes = password.GetBytes(Keysize / 8);
                        using (var symmetricKey = new RijndaelManaged())
                        {
                            symmetricKey.BlockSize = 256;
                            symmetricKey.Mode = CipherMode.CBC;
                            symmetricKey.Padding = PaddingMode.PKCS7;
                            using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes))
                            {
                                using (var memoryStream = new MemoryStream())
                                {
                                    using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                                    {
                                        cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                        cryptoStream.FlushFinalBlock();
                                        // Create the final bytes as a concatenation of the random salt bytes, the random iv bytes and the cipher bytes.
                                        var cipherTextBytes = saltStringBytes;
                                        cipherTextBytes = cipherTextBytes.Concat(ivStringBytes).ToArray();
                                        cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();
                                        memoryStream.Close();
                                        cryptoStream.Close();
                                        string encriptedstring = Convert.ToBase64String(cipherTextBytes);
                                        return SafeUrlEncoder(encriptedstring);
                                    }
                                }
                            }
                        }
                    }
                }


                public static string DecryptUrlFriend(string cipherText, string passPhrase)
                {

                    cipherText = SafeUrlDecoder(cipherText);

                    // Get the complete stream of bytes that represent:
                    // [32 bytes of Salt] + [32 bytes of IV] + [n bytes of CipherText]
                    var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText);
                    // Get the saltbytes by extracting the first 32 bytes from the supplied cipherText bytes.
                    var saltStringBytes = cipherTextBytesWithSaltAndIv.Take(Keysize / 8).ToArray();
                    // Get the IV bytes by extracting the next 32 bytes from the supplied cipherText bytes.
                    var ivStringBytes = cipherTextBytesWithSaltAndIv.Skip(Keysize / 8).Take(Keysize / 8).ToArray();
                    // Get the actual cipher text bytes by removing the first 64 bytes from the cipherText string.
                    var cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((Keysize / 8) * 2).Take(cipherTextBytesWithSaltAndIv.Length - ((Keysize / 8) * 2)).ToArray();

                    using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
                    {
                        var keyBytes = password.GetBytes(Keysize / 8);
                        using (var symmetricKey = new RijndaelManaged())
                        {
                            symmetricKey.BlockSize = 256;
                            symmetricKey.Mode = CipherMode.CBC;
                            symmetricKey.Padding = PaddingMode.PKCS7;
                            using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
                            {
                                using (var memoryStream = new MemoryStream(cipherTextBytes))
                                {
                                    using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                                    {
                                        var plainTextBytes = new byte[cipherTextBytes.Length];
                                        var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                        memoryStream.Close();
                                        cryptoStream.Close();
                                        return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                                    }
                                }
                            }
                        }
                    }
                }




                public static string Encrypt(string plainText, string passPhrase)
                {
                    // Salt and IV is randomly generated each time, but is preprended to encrypted cipher text
                    // so that the same Salt and IV values can be used when decrypting.  
                    var saltStringBytes = Generate256BitsOfRandomEntropy();
                    var ivStringBytes = Generate256BitsOfRandomEntropy();
                    var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
                    using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
                    {
                        var keyBytes = password.GetBytes(Keysize / 8);
                        using (var symmetricKey = new RijndaelManaged())
                        {
                            symmetricKey.BlockSize = 256;
                            symmetricKey.Mode = CipherMode.CBC;
                            symmetricKey.Padding = PaddingMode.PKCS7;
                            using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes))
                            {
                                using (var memoryStream = new MemoryStream())
                                {
                                    using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                                    {
                                        cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                        cryptoStream.FlushFinalBlock();
                                        // Create the final bytes as a concatenation of the random salt bytes, the random iv bytes and the cipher bytes.
                                        var cipherTextBytes = saltStringBytes;
                                        cipherTextBytes = cipherTextBytes.Concat(ivStringBytes).ToArray();
                                        cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();
                                        memoryStream.Close();
                                        cryptoStream.Close();
                                        return Convert.ToBase64String(cipherTextBytes);
                                    }
                                }
                            }
                        }
                    }
                }


                public static string Decrypt(string cipherText, string passPhrase)
                {

                    // Get the complete stream of bytes that represent:
                    // [32 bytes of Salt] + [32 bytes of IV] + [n bytes of CipherText]
                    var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText);
                    // Get the saltbytes by extracting the first 32 bytes from the supplied cipherText bytes.
                    var saltStringBytes = cipherTextBytesWithSaltAndIv.Take(Keysize / 8).ToArray();
                    // Get the IV bytes by extracting the next 32 bytes from the supplied cipherText bytes.
                    var ivStringBytes = cipherTextBytesWithSaltAndIv.Skip(Keysize / 8).Take(Keysize / 8).ToArray();
                    // Get the actual cipher text bytes by removing the first 64 bytes from the cipherText string.
                    var cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((Keysize / 8) * 2).Take(cipherTextBytesWithSaltAndIv.Length - ((Keysize / 8) * 2)).ToArray();

                    using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
                    {
                        var keyBytes = password.GetBytes(Keysize / 8);
                        using (var symmetricKey = new RijndaelManaged())
                        {
                            symmetricKey.BlockSize = 256;
                            symmetricKey.Mode = CipherMode.CBC;
                            symmetricKey.Padding = PaddingMode.PKCS7;
                            using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
                            {
                                using (var memoryStream = new MemoryStream(cipherTextBytes))
                                {
                                    using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                                    {
                                        var plainTextBytes = new byte[cipherTextBytes.Length];
                                        var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                        memoryStream.Close();
                                        cryptoStream.Close();
                                        return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                                    }
                                }
                            }
                        }
                    }
                }

                private static byte[] Generate256BitsOfRandomEntropy()
                {
                    var randomBytes = new byte[32]; // 32 Bytes will give us 256 bits.
                    using (var rngCsp = new RNGCryptoServiceProvider())
                    {
                        // Fill the array with cryptographically secure random bytes.
                        rngCsp.GetBytes(randomBytes);
                    }
                    return randomBytes;
                }

                public static class EncryptKey
                {
                    public static string ForgotPassword
                    {
                        get { return "KoodakSalam-FP"; }
                    }
                    public static string SignUp
                    {
                        get { return "KoodakSalam-SU"; }
                    }


                }
            }

            /// <summary>
            /// MD5 Hashing
            /// </summary>
            /// <param name="inputString"></param>
            /// <returns></returns>
            public static string GetHashString(string inputString)
            {
                MD5 md5Hash = MD5.Create();
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(inputString));

                StringBuilder sb = new StringBuilder();

                foreach (byte b in data)
                {
                    sb.Append(b.ToString("x2"));
                }

                return sb.ToString();
            }

            /// <summary>
            /// رمزنگاری UTF8
            /// </summary>
            /// <param name="input">رشته ای که می خواهید رمزنگاری نمایید</param>
            /// <returns></returns>
            public static string EncodeUTF8(string input)
            {
                return HttpUtility.HtmlEncode(input);
            }

            /// <summary>
            /// رمزگشایی UTF8
            /// </summary>
            /// <param name="input">رشته ای که میخواهید رمزگشایی نمایید</param>
            /// <returns></returns>
            public static string DecodeUTF8(string input)
            {
                StringWriter myWriter = new StringWriter();
                HttpUtility.HtmlDecode(input, myWriter);
                return myWriter.ToString();
            }
        }




        public static string RandomString(int length)
        {

            Random random = new Random();

            const string chars = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public static string RandomNumber(int length)
        {

            Random random = new Random();

            const string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

       









        public static class CustomDateTime
        {

            public static PersianCalendar Persian = new PersianCalendar();
            static PersianDateTime pdt = new PersianDateTime(DateTime.Now);

            #region [ Public Method(s) ]
            /// <summary>
            /// تاریخ
            /// </summary>
            /// <param name="datetype">نوع تاریخ</param>
            /// <returns></returns>
            public static string GetShamsiDate(Enumerations.DateType datetype)
            {
                switch (datetype)
                {
                    case Enumerations.DateType.Short:
                        return pdt.Year + "/" + String.Format("{0:00}", pdt.Month) + "/" + String.Format("{0:00}", pdt.Day);
                        break;
                    default:
                        return pdt.Year + "/" + String.Format("{0:00}", pdt.Month) + "/" + String.Format("{0:00}", pdt.Day);
                }
            }

            /// <summary>
            /// تبدیل تاریخ شمسی به میلادی
            /// </summary>
            /// <param name="ShamsiDate">تاریخ شمسی</param>
            /// <returns></returns>
            public static DateTime ShamsiToMiladi(string ShamsiDate)
            {
                int year = Int32.Parse(ShamsiDate.Substring(0, 4));
                int month = Int32.Parse(ShamsiDate.Substring(5, 2));
                int day = Int32.Parse(ShamsiDate.Substring(8, 2));
                DateTime dt = Persian.ToDateTime(year, month, day, 0, 0, 0, 0);
                return dt;
            }
            public static string MiladiToShamsi(DateTime MiladiDate)
            {
                PersianCalendar shamsi = new PersianCalendar();
                DateTime sh;
                string strdate = null;
                strdate = MiladiDate.ToString("yyyy/MM/dd");
                sh = DateTime.Parse(strdate);
                string ysh = String.Format("{0:00}", shamsi.GetYear(sh));
                string msh = String.Format("{0:00}", shamsi.GetMonth(sh));
                string dsh = String.Format("{0:00}", shamsi.GetDayOfMonth(sh));
                var a = shamsi.GetDayOfWeek(sh);
                return ysh + "/" + msh + "/" + dsh;
            }
            public static bool IsValidDate(string date)
            {
                Regex pattern = new Regex("^\\d{ 4 } /\\d{ 2}/\\d{ 2}$");
                Regex[] arrPattern = new Regex[] {
              new Regex("^\\d{4}/\\d{2}/\\d{2}$"),
              new Regex("^\\d{ 4 } /\\d{ 2}/\\d{ 1}$"),
              new Regex("^\\d{ 4 } /\\d{ 1}/\\d{ 2}$"),
              new Regex("^\\d{ 4 } /\\d{ 1}/\\d{ 1}$"),
              new Regex("^\\d{ 2 } /\\d{ 2}/\\d{ 2}$"),
              new Regex("^\\d{ 2 } /\\d{ 2}/\\d{ 1}$"),
              new Regex("^\\d{ 2 } /\\d{ 1}/\\d{ 2}$"),
              new Regex("^\\d{ 2 } /\\d{ 1}/\\d{ 1}")
          };
                int kabise = 1387;
                int year = 0;
                int mounth = 0;
                int day = 0; bool flag = false;
                for (int i = 0; i < arrPattern.Length; i++)
                {
                    if (arrPattern[i].IsMatch(date))
                        flag = true;
                }
                if (flag == false) return flag;
                //جدا کننده تاریخ می تواند یکی از این کاراکترها باشد
                string[] splitDate = date.Split('/', '-', ':');
                year = Convert.ToInt32(splitDate[0]);
                mounth = Convert.ToInt32(splitDate[1]);
                day = Convert.ToInt32(splitDate[2]);
                if (mounth > 12 || mounth <= 0)
                    flag = false;
                else
                {
                    if (mounth < 7)
                    {
                        if (day > 31)
                        {
                            flag = false;
                        }
                    }
                    if (mounth == 12)
                    {
                        int t = (year - kabise) % 4;
                        if ((year - kabise) % 4 == 0)
                        {
                            if (day >= 31)
                                flag = false;
                        }
                        else if (day >= 30)
                            flag = false;
                    }
                    else
                    {
                        if (day > 30)
                            flag = false;
                    }
                }
                return flag;
            }



            public static ShamsiTime GetCurrent(DateTime miladiDate)
            {
                PersianCalendar shamsi = new PersianCalendar();
                DateTime sh;
                string strdate = null;
                strdate = miladiDate.ToString("yyyy/MM/dd");
                sh = DateTime.Parse(strdate);
                string ysh = String.Format("{0:00}", shamsi.GetYear(sh));
                string msh = String.Format("{0:00}", shamsi.GetMonth(sh));
                string dsh = String.Format("{0:00}", shamsi.GetDayOfMonth(sh));
                int wsh = (int)DateTime.Now.DayOfWeek;

                return new ShamsiTime
                {
                    FullStringTime = getWeek(wsh.ToString()) + " " + dsh + " " + getMounth(msh) + " " + ysh,
                    FullNumeralTime = ysh + "/" + msh + "/" + dsh,
                    Day = dsh,
                    Month = msh,
                    Year = ysh,
                    MonthName = getMounth(msh),
                    WeekDayName = getWeek(wsh.ToString())
                };
            }


            public static string RelativeShamsiDate(DateTime yourDate)
            {
                const int SECOND = 1;
                const int MINUTE = 60 * SECOND;
                const int HOUR = 60 * MINUTE;
                const int DAY = 24 * HOUR;
                const int MONTH = 30 * DAY;
                string retrunDate = "";
                var ts = new TimeSpan(DateTime.UtcNow.Ticks - yourDate.Ticks);
                double delta = Math.Abs(ts.TotalSeconds);

                if (delta < 1 * MINUTE)
                    retrunDate = ts.Seconds == 1 ? "چند لحظه پیش" : ts.Seconds.ToString().Replace("-", "") + " ثانیه پیش";

                if (delta < 2 * MINUTE)
                    return "یک دقیقه پیش";

                if (delta < 45 * MINUTE)
                    return ts.Minutes.ToString().Replace("-", "") + " دقیقه پیش";

                if (delta < 90 * MINUTE)
                    return "یک ساعت پیش";

                if (delta < 24 * HOUR)
                    return ts.Hours.ToString().Replace("-", "") + " ساعت پیش";

                if (delta < 48 * HOUR)
                    return "دیروز";

                if (delta < 30 * DAY)
                    return ts.Days.ToString().Replace("-", "") + " روز پیش";

                if (delta < 12 * MONTH)
                {
                    int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                    return months <= 1 ? "یک ماه پیش" : months.ToString().Replace("-", "") + " ماه پیش";
                }
                else
                {
                    int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
                    return years <= 1 ? "یک سال پیش" : years.ToString().Replace("-", "") + " سال پیش";
                }

            }


            public static string getMounth(string month)
            {
                switch (month)
                {
                    case "01":
                        return "فروردین";
                    case "02":
                        return "اردیبهشت";
                    case "03":
                        return "خرداد";
                    case "04":
                        return "تیر";
                    case "05":
                        return "مرداد";
                    case "06":
                        return "شهریور";
                    case "07":
                        return "مهر";
                    case "08":
                        return "آبان";
                    case "09":
                        return "آذر";
                    case "10":
                        return "دی";
                    case "11":
                        return "بهمن";
                    case "12":
                        return "اسفند";
                    default:
                        return "تعریف نشده";
                }
            }
            public static string getWeek(string week)
            {
                switch (week)
                {
                    case "6":
                        return "شنبه";
                    case "7":
                        return "یک شنبه";
                    case "1":
                        return "دوشنبه";
                    case "2":
                        return "سه شنبه";
                    case "3":
                        return "چهارشنبه";
                    case "4":
                        return "پنج شنبه";
                    case "5":
                        return "جمعه";
                    default:
                        return "تعریف نشده";
                }
            }
            #endregion
        }

    }
}