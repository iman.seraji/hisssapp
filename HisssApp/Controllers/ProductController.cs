﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BLL.Models;
using DAL;
using Newtonsoft.Json.Linq;

namespace HisssApp.Controllers
{
    public class ProductController : ApiController
    {


        HisssAppDBDataContext HisssAppDB = new HisssAppDBDataContext();


        public List<Brand> GetBrandsList()
        {

            var Brands = (from BR in HisssAppDB.tblBrands
                          select new Brand()
                         {
                             ID = BR.ID,
                             BrandName_FA = BR.BrandName_FA,
                             BrandName_EN = BR.BrandName_EN,
                             BrandImage = BR.BrandImage,
                             BrandIsAvtive = BR.BrandIsActive,
                             BrandPriority = BR.BrandPriority ?? 0
                         }).ToList();




            return Brands.ToList();

        }
        public List<Brand> GetBrandsListByCat(int id)
        {

            var Brands = (from BR in HisssAppDB.vwBrandsByCats
                          where BR.CatID == id
                          select new Brand()
                          {
                              ID = BR.ID ?? 0,
                              BrandName_FA = BR.BrandName_FA,
                              BrandName_EN = BR.BrandName_EN,
                              BrandImage = BR.BrandImage,
                              BrandIsAvtive = BR.BrandIsActive ?? 0,
                              BrandPriority = BR.BrandPriority ?? 0
                          }).ToList();




            return Brands.ToList();

        }
        public List<ProductCategory> GetProductCats()
        {

            var ProductCats = (from BR in HisssAppDB.tblProduct_Categories
                               select new ProductCategory()
                          {
                              ID = BR.ID,
                              CategoryName_FA = BR.CategoryName_FA,
                              CategoryName_EN = BR.CategoryName_EN,
                              CategoryPriority = BR.CategoryPriority ?? 0,
                              RegDate = BR.RegDate ?? DateTime.Now

                          }).ToList();




            return ProductCats.ToList();

        }
        public List<Product> GetProductList()
        {

            var Product = (from PL in HisssAppDB.tblProducts
                           orderby PL.RegDate descending
                           select new Product()
                           {
                               ID = PL.ID,
                               BrandID = PL.BrandID,
                               SubCategoryID = PL.SubCategoryID,
                               ProductCode = PL.ProductCode,
                               ProductName_FA = PL.ProductName_FA,
                               ProductName_EN = PL.ProductName_EN,
                               ProductGender = PL.ProductGender,
                               ProductDesc = PL.ProductDesc,
                               LastPrice = PL.LastPrice ?? "0",
                               ProductMainImage = PL.ProductMainImage,
                               Product3DImage = PL.Product3DImage,
                               ProductPriority = PL.ProductPriority ?? 0,
                               RegDate = PL.RegDate ?? DateTime.Now

                           }).ToList();




            return Product.ToList();

        }
        public List<Product> GetProductList(int id)
        {

            var Product = (from PL in HisssAppDB.tblProducts
                           where PL.SubCategoryID == id
                           orderby PL.RegDate descending
                           select new Product()
                           {
                               ID = PL.ID,
                               BrandID = PL.BrandID,
                               SubCategoryID = PL.SubCategoryID,
                               ProductCode = PL.ProductCode,
                               ProductName_FA = PL.ProductName_FA,
                               ProductName_EN = PL.ProductName_EN,
                               ProductGender = PL.ProductGender,
                               ProductDesc = PL.ProductDesc,
                               LastPrice = PL.LastPrice ?? "0",
                               ProductMainImage = PL.ProductMainImage,
                               Product3DImage = PL.Product3DImage,
                               ProductPriority = PL.ProductPriority ?? 0,
                               RegDate = PL.RegDate ?? DateTime.Now

                           }).ToList();




            return Product.ToList();

        }
        public ProductDetail GetProductDetail(int id)
        {








            var ProductPrice = (from PP in HisssAppDB.tblProduct_prices
                                where PP.ProductID == id
                                orderby PP.RegDate
                                select PP).FirstOrDefault();





            if (ProductPrice != null)
            {


                var qrProductDetail = (from PL in HisssAppDB.tblProducts
                                       where PL.ID == id
                                       select new ProductDetail()
                                       {
                                           ID = PL.ID,
                                           BrandID = PL.BrandID,
                                           SubCategoryID = PL.SubCategoryID,
                                           ProductCode = PL.ProductCode,
                                           ProductName_FA = PL.ProductName_FA,
                                           ProductName_EN = PL.ProductName_EN,
                                           ProductGender = PL.ProductGender,
                                           ProductDesc = PL.ProductDesc,
                                           ProductMainImage = PL.ProductMainImage,
                                           Product3DImage = PL.Product3DImage,
                                           ProductPriority = PL.ProductPriority ?? 0,
                                           Price = ProductPrice.Price ?? "0",
                                           LastPrice = ProductPrice.LastPrice ?? "0",
                                           SalePercent = ProductPrice.SalePercent ?? 0,
                                           CashBack = ProductPrice.CashBack ?? "0",
                                           RegDate = PL.RegDate ?? DateTime.Now

                                       }).FirstOrDefault();




                return qrProductDetail;

            }
            else
            {
                var qrProductDetail = (from PL in HisssAppDB.tblProducts
                                       where PL.ID == id
                                       select new ProductDetail()
                                       {
                                           ID = PL.ID,
                                           BrandID = PL.BrandID,
                                           SubCategoryID = PL.SubCategoryID,
                                           ProductCode = PL.ProductCode,
                                           ProductName_FA = PL.ProductName_FA,
                                           ProductName_EN = PL.ProductName_EN,
                                           ProductGender = PL.ProductGender,
                                           ProductDesc = PL.ProductDesc,
                                           ProductMainImage = PL.ProductMainImage,
                                           Product3DImage = PL.Product3DImage,
                                           ProductPriority = PL.ProductPriority ?? 0,
                                           Price = "0",
                                           LastPrice = "0",
                                           SalePercent = 0,
                                           CashBack = "0",
                                           RegDate = PL.RegDate ?? DateTime.Now

                                       }).FirstOrDefault();




                return qrProductDetail;
            }






        }
        public List<Product> ProductsFirstList(JObject Data)
        {



            dynamic PData = Data;


            int intGenders = PData.GendersID;
            int intSubCategoryID = PData.SubCategoryID;


            var Product = (from PL in HisssAppDB.vwProducts
                           where PL.SubCategoryID == intSubCategoryID && PL.GenderID == intGenders
                           orderby PL.RegDate descending
                           select new Product()
                           {
                               ID = PL.ID,
                               BrandID = PL.BrandID,
                               SubCategoryID = PL.SubCategoryID,
                               ProductCode = PL.ProductCode,
                               ProductName_FA = PL.ProductName_FA,
                               ProductName_EN = PL.ProductName_EN,
                               ProductGender = PL.ProductGender,
                               ProductDesc = PL.ProductDesc,
                               LastPrice = PL.LastPrice ?? "0",
                               ProductMainImage = PL.ProductMainImage,
                               Product3DImage = PL.Product3DImage,
                               ProductPriority = PL.ProductPriority ?? 0,
                               RegDate = PL.RegDate ?? DateTime.Now

                           }).ToList();




            return Product.ToList();


        }
        [HttpPost]
        public List<Product> ProductsFiltering(JObject FilteringData)
        {


            dynamic RData = FilteringData;

            string strBrands = RData.Brands;
            string strGenders = RData.Genders;


            int intSubCategoryID = RData.SubCategoryID;
            int intBrandID = RData.BrandID;






            var Product = (from PL in HisssAppDB.tblProducts
                           where PL.SubCategoryID == intSubCategoryID && PL.BrandID == intBrandID
                           orderby PL.RegDate descending
                           select new Product()
                           {
                               ID = PL.ID,
                               BrandID = PL.BrandID,
                               SubCategoryID = PL.SubCategoryID,
                               ProductCode = PL.ProductCode,
                               ProductName_FA = PL.ProductName_FA,
                               ProductName_EN = PL.ProductName_EN,
                               ProductGender = PL.ProductGender,
                               ProductDesc = PL.ProductDesc,
                               LastPrice = PL.LastPrice ?? "0",
                               ProductMainImage = PL.ProductMainImage,
                               Product3DImage = PL.Product3DImage,
                               ProductPriority = PL.ProductPriority ?? 0,
                               RegDate = PL.RegDate ?? DateTime.Now

                           }).ToList();




            return Product.ToList();

        }
        public List<Product> GetProductListByBrandID(int id)
        {

            var Product = (from PL in HisssAppDB.tblProducts
                           where PL.BrandID == id
                           orderby PL.RegDate descending
                           select new Product()
                           {
                               ID = PL.ID,
                               BrandID = PL.BrandID,
                               SubCategoryID = PL.SubCategoryID,
                               ProductCode = PL.ProductCode,
                               ProductName_FA = PL.ProductName_FA,
                               ProductName_EN = PL.ProductName_EN,
                               ProductGender = PL.ProductGender,
                               ProductDesc = PL.ProductDesc,
                               LastPrice = PL.LastPrice ?? "0",
                               ProductMainImage = PL.ProductMainImage,
                               Product3DImage = PL.Product3DImage,
                               ProductPriority = PL.ProductPriority ?? 0,
                               RegDate = PL.RegDate ?? DateTime.Now

                           }).ToList();




            return Product.ToList();

        }
        public List<ProductCategory> GetProductCatByGender(int id)
        {

            var ProductCategory = (from PC in HisssAppDB.vwProductsCats
                                   where PC.GenderID == id
                                   orderby PC.RegDate descending
                                   select new ProductCategory()
                                   {
                                       ID = PC.ID ?? 0,
                                       CategoryName_FA = PC.CategoryName_FA,
                                       CategoryName_EN = PC.CategoryName_EN,
                                       CategoryPriority = PC.CategoryPriority ?? 0,
                                       RegDate = PC.RegDate ?? DateTime.Now

                                   }).ToList();




            return ProductCategory.ToList();

        }
        public List<ProductSubCategory> GetProductSubCat(int id)
        {

            var ProductSubCategory = (from PS in HisssAppDB.tblProduct_SubCategories
                                      where PS.CategoryID == id
                                      orderby PS.RegDate descending
                                      select new ProductSubCategory()
                                      {
                                          ID = PS.ID,
                                          SubCategoryName_FA = PS.SubCategoryName_FA,
                                          SubCategoryName_EN = PS.SubCategoryName_EN,
                                          SubCategoryPriority = PS.SubCategoryPriority ?? 0,
                                          RegDate = PS.RegDate ?? DateTime.Now

                                      }).ToList();




            return ProductSubCategory.ToList();

        }


    }
}
