﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BLL.Models;
using DAL;
using HisssApp.Services;
using Newtonsoft.Json.Linq;

namespace HisssApp.Controllers
{
    public class UserController : ApiController
    {
      public static  HisssAppDBDataContext HisssAppDB = new HisssAppDBDataContext();


          [HttpPost]
        public  Tuple<bool, string > RegisterWithPhone(JObject UserData)
        {
            try
            {

                Guid UserID = Guid.NewGuid();

                dynamic RegData = UserData;

                string strUserEmail = RegData.Email ?? "";
                string strUserMobile = RegData.Mobile;
                strUserMobile ="+98"+ strUserMobile.Substring(strUserMobile.Length - 10, 10);
                string strEventGift = RegData.EventGift ?? "";
                string registerIP = RegData.RegIP ?? "";
                int DeviceID = Convert.ToInt32(RegData.DeviceTypeID) ?? 1;


                //sakhtan json device baraye register

                dynamic DeviceInfo = new JObject();

                DeviceInfo.DeviceID = RegData.DeviceID;
                DeviceInfo.DeviceTypeID = RegData.DeviceTypeID;
                DeviceInfo.DeviceNotifictionID = RegData.DeviceNotifictionID ?? "";
                DeviceInfo.DeviceIP = RegData.RegIP ?? "";
                DeviceInfo.DeviceBrand = RegData.DeviceBrand ?? "";
                DeviceInfo.DeviceModel = RegData.DeviceModel ?? "";
                DeviceInfo.DeviceOsVersion = RegData.DeviceOsVersion ?? "";
                DeviceInfo.DeviceScreenSize = RegData.DeviceScreenSize ?? "";
                DeviceInfo.DeviceStatusID = RegData.DeviceStatusID;
                DeviceInfo.DeviceApppVersion = RegData.ApppVersion;


                 var UserInfo = (from u in HisssAppDB.tblUsers where u.Mobile == strUserMobile select u).FirstOrDefault();


                if (UserInfo != null)
                {

                     MessageServices.SendSMS(strUserMobile, string.Format("کد ورود شما : {0}\nبه خانواده بزرگ هیس اپ خوش آمدید .", UserInfo.RegCode));


                    DeviceInfo.UserID = UserInfo.ID;
                    if (RegData.DeviceID != null)
                    {
                        RegisterUserDevice(DeviceInfo);
                    }


                    return new Tuple<bool, string  >(true, UserInfo.ID.ToString()  );

                }
                else
                {

                    string strRegCode = Common.GeneralClass.RandomNumber(5);


                    tblUser UserObject = new tblUser()
                    {

                        ID = UserID,
                        Mobile = strUserMobile,
                        IsActive = false,
                        RegCode = strRegCode,
                        RegDate = DateTime.Now
                    };




                    HisssAppDB.tblUsers.InsertOnSubmit(UserObject);


                    HisssAppDB.SubmitChanges();



                    DeviceInfo.UserID = UserID;
                    if (RegData.DeviceID != null)
                    {
                        RegisterUserDevice(DeviceInfo);
                    }


                    MessageServices.SendSMS(strUserMobile,string.Format("کد ورود شما : {0}\nبه خانواده بزرگ هیس اپ خوش آمدید .", strRegCode));

                    return new Tuple<bool, string  >(true, UserID.ToString()  );

                }





            }
            catch (Exception e)
            {
                MessageServices.SendErrToAdmin(e.Message.ToString(), "Register Errore =  ");
                return new Tuple<bool, string  >(false, e.ToString()  );
            }
        }



        public static Tuple<bool, string> RegisterUserDevice(JObject DeviceInfo)
        {

            try
            {


                Guid ID = Guid.NewGuid();

                dynamic DeviceData = DeviceInfo;


                Guid UserID = DeviceData.UserID;
                string strDeviceID = DeviceData.DeviceID;
                int intDeviceTypeID = DeviceData.DeviceTypeID;
                string strDeviceNotifictionID = DeviceData.DeviceNotifictionID ?? "";
                string strDeviceIP = DeviceData.DeviceIP ?? "";
                string strDeviceBrand = DeviceData.DeviceBrand ?? "";
                string strDeviceModel = DeviceData.DeviceModel ?? "";
                string strDeviceOsVersion = DeviceData.DeviceOsVersion ?? "";
                string strDeviceScreenSize = DeviceData.DeviceScreenSize ?? "";
                int intDeviceStatusID = DeviceData.DeviceStatusID;
                string strApppVersion = DeviceData.ApppVersion;


                var qrDevice =
                    (from dv in HisssAppDB.tblUser_Devices where dv.DeviceID == strDeviceID select dv).FirstOrDefault();


                if (qrDevice != null)
                {

                    qrDevice.UserID = UserID;
                    qrDevice.DeviceIP = strDeviceIP;
                    qrDevice.DeviceNotifictionID = strDeviceNotifictionID;
                    qrDevice.DeviceOsVersion = strDeviceOsVersion;
                    qrDevice.DeviceStatusID = intDeviceStatusID;





                }
                else
                {



                    tblUser_Device UserDeviceObject = new tblUser_Device()
                    {

                        ID = ID,
                        UserID = UserID,
                        DeviceID = strDeviceID,
                        DeviceTypeID = intDeviceTypeID,
                        DeviceNotifictionID = strDeviceNotifictionID,
                        DeviceIP = strDeviceIP,
                        DeviceBrand = strDeviceBrand,
                        DeviceModel = strDeviceModel,
                        DeviceOsVersion = strDeviceOsVersion,
                        DeviceScreenSize = strDeviceScreenSize,
                        DeviceStatusID = intDeviceStatusID,
                        DeviceAppVersion = strApppVersion,
                        RegDate = DateTime.Now
                    };

                    HisssAppDB.tblUser_Devices.InsertOnSubmit(UserDeviceObject);

                }

                HisssAppDB.SubmitChanges();








                return new Tuple<bool, string>(true, ID.ToString());

            }
            catch (Exception e)
            {

                return new Tuple<bool, string>(false, e.Message.ToString());
            }



        }



    }
}
