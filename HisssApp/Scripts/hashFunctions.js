﻿function Hash() {
    return {
        getUrlHash: function () {
            var a = location.hash;
            return String(a);
        },
        getUrlParameter: function (sParam) {
            var sPageUrl = decodeURIComponent(window.location.hash);
            var sUrlVariables = sPageUrl.split('/');
            var sParameterName;
            var i;

            for (i = 0; i < sUrlVariables.length; i++) {
                sParameterName = sUrlVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? "" : sParameterName[1];
                }
            }
            return "";
        },
        setUrlParameter: function (paramList) {
            var sPageUrl = decodeURIComponent(window.location.hash);
            var sUrlVariables = sPageUrl.split('/');
            var sParameterName;
            var i;
            var findedCount = 0;
            var newUrl = "";
            $.each(paramList, function (index, paramters) {
                findedCount = 0;
                var sParam = paramters.sParam, sParamValue = paramters.sParamValue;

                for (i = 0; i < sUrlVariables.length; i++) {

                    sParameterName = sUrlVariables[i].split('=');


                    if (sParameterName[0] === sParam) {

                        sUrlVariables[i] = sParam + "=" + sParamValue;
                        findedCount++;
                    }
                }


                if (findedCount === 0) {
                    sUrlVariables.push(sParam + '=' + sParamValue);
                }
            });

            for (i = 0; i < sUrlVariables.length; i++) {

                var tempList = sUrlVariables[i].split('=');
                if (typeof tempList[1] !== 'undefined' && tempList[1] != "") {
                    newUrl += "/" + tempList[0] + "=" + tempList[1];
                }
            }
            window.location.hash = newUrl;
        },
        addUrlParameter: function (sParam, sParamValue, sParamText1, sParamText2) {

         

            var sPageUrl = decodeURIComponent(window.location.hash);
            var sUrlVariables = sPageUrl.split('/');
            var sParameterName;
            var i;
            var findedCount = 0;
            var newUrl = "";

            findedCount = 0;


            for (i = 0; i < sUrlVariables.length; i++) {

                sParameterName = sUrlVariables[i].split('=');


                if (sParameterName[0] === sParam) {

                    sUrlVariables[i] = sParam + "=" + sParamValue;
                    findedCount++;
                }
            }


            if (findedCount === 0) {

                sUrlVariables.push(sParam + '=' + sParamValue);
            }

            for (i = 0; i < sUrlVariables.length; i++) {

                var tempList = sUrlVariables[i].split('=');
                if (typeof tempList[1] !== 'undefined' && tempList[1] != "") {
                    newUrl += "/" + tempList[0] + "=" + tempList[1];
                    if (typeof sParamText1 !== 'undefined' && typeof sParamText2 !== 'undefined') {
                        Hash().addTag(sParam, sParamValue, sParamText1, sParamText2);
                    }
                }
            }
            window.location.hash = newUrl;
        },
        addTag: function (sParam, sParamValue, sParamText1, sParamText2) {
           
            var isExist = false;
            $('#tags-container li').each(function () {
                var current = $(this).find('span').attr('data-value');
                if (current.split('-')[0] == sParam) {
                    isExist = true;
                    $(this).remove();

                    if (sParamText1 !== '' && sParamText2 !== '') {

                        $('#tags-container').append('<li><span class="btn" data-value="' + sParam + '-' + sParamValue + '">' + sParamText1 + ':' + sParamText2 + '<span class="BreadCrumbClose" onclick="Hash().removeTag(this,\'' + sParam + '-' + sParamValue + '\')" data-value="' + sParam + '-' + sParamValue + '"></span></span></li>');
                    }

                }

            });
            if (!isExist && sParamText1 !== '' && sParamText2 !== '') {
                
                $('#tags-container').append('<li><span class="btn" data-value="' + sParam + '-' + sParamValue + '">' + sParamText1 + ':' + sParamText2 + '<span class="BreadCrumbClose" onclick="Hash().removeTag(this,\'' + sParam + '-' + sParamValue + '\')" data-value="' + sParam + '-' + sParamValue + '"></span></span></li>');

            }

        },
        removeTag: function (current, data) {
            $(current).parents('li').remove();
            var sparam = data.split("-")[0];
            Hash().addUrlParameter(sparam,'');

        }
    }
}


var url = window.location.href;
function QueryString() {
    return {
        getUrlHash: function () {
            var a = location.hash;
            return String(a);
        },
        getUrlParameter: function (sParam) {
            var sPageUrl = decodeURIComponent(url.split('?')[1]);
           
            var sUrlVariables = sPageUrl.split('/');
            var sParameterName;
            var i;

            for (i = 0; i < sUrlVariables.length; i++) {
                sParameterName = sUrlVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? "" : sParameterName[1];
                }
            }
            return "";
        },
        setUrlParameter: function (paramList) {
            var sPageUrl = decodeURIComponent(url.split('?')[1]);
            var sUrlVariables = sPageUrl.split('/');
            var sParameterName;
            var i;
            var findedCount = 0;
            var newUrl = "";
            $.each(paramList, function (index, paramters) {
                findedCount = 0;
                var sParam = paramters.sParam, sParamValue = paramters.sParamValue;

                for (i = 0; i < sUrlVariables.length; i++) {

                    sParameterName = sUrlVariables[i].split('=');


                    if (sParameterName[0] === sParam) {

                        sUrlVariables[i] = sParam + "=" + sParamValue;
                        findedCount++;
                    }
                }


                if (findedCount === 0) {
                    sUrlVariables.push(sParam + '=' + sParamValue);
                }
            });

            for (i = 0; i < sUrlVariables.length; i++) {

                var tempList = sUrlVariables[i].split('=');
                if (typeof tempList[1] !== 'undefined' && tempList[1] != "") {
                    newUrl += "/" + tempList[0] + "=" + tempList[1];
                }
            }
            window.location.hash = newUrl;
        },
        addUrlParameter: function (sParam, sParamValue, sParamText1, sParamText2) {



            var sPageUrl = decodeURIComponent(url.split('?')[1]);
            var sUrlVariables = sPageUrl.split('/');
            var sParameterName;
            var i;
            var findedCount = 0;
            var newUrl = "";

            findedCount = 0;


            for (i = 0; i < sUrlVariables.length; i++) {

                sParameterName = sUrlVariables[i].split('=');


                if (sParameterName[0] === sParam) {

                    sUrlVariables[i] = sParam + "=" + sParamValue;
                    findedCount++;
                }
            }


            if (findedCount === 0) {

                sUrlVariables.push(sParam + '=' + sParamValue);
            }

            for (i = 0; i < sUrlVariables.length; i++) {

                var tempList = sUrlVariables[i].split('=');
                if (typeof tempList[1] !== 'undefined' && tempList[1] != "") {
                    newUrl += "/" + tempList[0] + "=" + tempList[1];
                    if (typeof sParamText1 !== 'undefined' && typeof sParamText2 !== 'undefined') {
                        Hash().addTag(sParam, sParamValue, sParamText1, sParamText2);
                    }
                }
            }
            window.location.hash = newUrl;
        },
        addTag: function (sParam, sParamValue, sParamText1, sParamText2) {

            var isExist = false;
            $('#tags-container li').each(function () {
                var current = $(this).find('span').attr('data-value');
                if (current.split('-')[0] == sParam) {
                    isExist = true;
                    $(this).remove();

                    if (sParamText1 !== '' && sParamText2 !== '') {

                        $('#tags-container').append('<li><span class="btn" data-value="' + sParam + '-' + sParamValue + '">' + sParamText1 + ':' + sParamText2 + '<span class="BreadCrumbClose" onclick="Hash().removeTag(this,\'' + sParam + '-' + sParamValue + '\')" data-value="' + sParam + '-' + sParamValue + '"></span></span></li>');
                    }

                }

            });
            if (!isExist && sParamText1 !== '' && sParamText2 !== '') {

                $('#tags-container').append('<li><span class="btn" data-value="' + sParam + '-' + sParamValue + '">' + sParamText1 + ':' + sParamText2 + '<span class="BreadCrumbClose" onclick="Hash().removeTag(this,\'' + sParam + '-' + sParamValue + '\')" data-value="' + sParam + '-' + sParamValue + '"></span></span></li>');

            }

        },
        removeTag: function (current, data) {
            $(current).parents('li').remove();
            var sparam = data.split("-")[0];
            Hash().addUrlParameter(sparam, '');

        }
    }
}