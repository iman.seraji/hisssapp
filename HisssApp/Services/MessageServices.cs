﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;
using HisssApp.Common;
using BLL;
using File = System.IO.File;

namespace HisssApp.Services
{
    public class MessageServices
    {

        /// <summary>
        /// ارسال ایمیل - فراموشی رمز عبور 
        /// </summary>
        public static void GetForgetPasswordEmail(string toAddress, string recoverLink, string sendDate, string Email, string userFullName)
        {

            EmailStructure emailStructure = new EmailStructure(Constants.EmailSubject.ForgetPassword, toAddress);
            emailStructure.BodyFileName = Constants.TemplateFiles.UserEmailForgetPass;
            emailStructure.Body = emailStructure.Body.Replace("$UserFullName$", userFullName).Replace("$Email$", Email).Replace("$Date$", sendDate).Replace("$RecoverLink$", recoverLink);
            SendEmail(emailStructure);
        }



        public static void SendErrToAdmin(string ErroreText, string ErrorePart)
        {

            EmailStructure emailStructure = new EmailStructure(Constants.EmailSubject.ErroreToAdmin, "iman.seraji@gmail.com");
            emailStructure.Body = ErrorePart + "<br>" + ErroreText;
            SendEmail(emailStructure);
        }



        public static void SendEmail(EmailStructure emailInfo)
        {
            try
            {
                MailMessage mail = new MailMessage();
                foreach (var email in emailInfo.To)
                {
                    //mail.To.Add(GetValidAddress(emailInfo.To));
                    mail.To.Add(email);
                    mail.From = new MailAddress("support@gheseland.com", emailInfo.DisplayName = "پشتیبانی وب سایت هیس اپ");
                    mail.Subject = emailInfo.Subject;
                    mail.Body = emailInfo.Body;
                    mail.IsBodyHtml = true;
                    mail.Priority = MailPriority.High;
                    SmtpClient client = new SmtpClient();
                    client.Send(mail);
                }
            }

            catch (Exception ex)
            {



                EmailStructure emailStructure = new EmailStructure(Constants.EmailSubject.ForgetPassword, "iman.seraji@gmail.com");

                emailStructure.Body = ex.ToString();
                SendEmail(emailStructure);


                string alert = ex.ToString();
            }
        }



        public static void SendPaymentNotification(string UserID, string Price, string PaymentFrom)
        {

            EmailStructure emailStructure = new EmailStructure(PaymentFrom, "iman.seraji@gmail.com");
            emailStructure.Body = string.Format("UserID : {0} <br> Price : {1} <br> Payment Succes ..........", UserID, Price);

            SendEmail(emailStructure);
        }

       
        public static void SendSMS(string Mobile, string Message)
        {
            try
            {



                string html = string.Empty;
                string url = string.Format("https://negar.armaghan.net/sms/url_send.html?originator={0}&destination={1}&content={2}&password={3}&username={4}", BLL.Configuration.Sms.Armaghan.PhoneLine1, Mobile, Message, BLL.Configuration.Sms.Armaghan.PassWord, BLL.Configuration.Sms.Armaghan.UserName);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();


              //  sornaserviceSoapClient smssorna = new sornaserviceSoapClient();
              //  smssorna.SingleSMSEngine(Convert.ToInt16(Configuration.Sms.Sorna.SubscriptionCode), Configuration.Sms.Sorna.UserName, Configuration.Sms.Sorna.PassWord, Mobile, Message);
                //smssorna.SingleSMSEngine(Convert.ToInt16(ConfigurationManager.AppSettings["SornaPortalCode"]), ConfigurationManager.AppSettings["SornaUserName"], ConfigurationManager.AppSettings["SornaPassword"].ToString(), Mobile, Message);

            }
            catch (Exception ex)
            {
                string message = "پیام کوتاه برای شماره ی " + Mobile + "با موفقیت ارسال نشد لطفا بررسی شود<br>متن ارور : " + ex.Message;
                // SendEmail("iman.seraji@gmail.com", "ورزشان - مشکل در ارسال پیامک", message);
            }
        }


    }
}
