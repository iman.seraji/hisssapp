﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using HisssApp.Common;
using BLL.Models;


namespace HisssApp.Services
{
    public static class UtilityServices
    {
        public static string ToShamsiDate(this DateTime value)
        {
            PersianCalendar shamsi = new PersianCalendar();
            string strdate = null;
            strdate = value.ToString("yyyy/MM/dd");
            var sh = DateTime.Parse(strdate);
            string ysh = String.Format("{0:00}", shamsi.GetYear(sh));
            string msh = String.Format("{0:00}", shamsi.GetMonth(sh));
            string dsh = String.Format("{0:00}", shamsi.GetDayOfMonth(sh));
            return ysh + "/" + msh + "/" + dsh;
        }

        public static int DefrenceBetweenYears(string FirstDate, string EndDate)
        {


            System.Globalization.PersianCalendar persia = new System.Globalization.PersianCalendar();


            Int16 StartYear = Convert.ToInt16(FirstDate.Substring(0, 4));
            Int16 StartMonth = Convert.ToInt16(FirstDate.Substring(5, 2));
            Int16 StartDay = Convert.ToInt16(FirstDate.Substring(8, 2));

            Int16 EndYear = Convert.ToInt16(EndDate.Substring(0, 4));
            Int16 EndMonth = Convert.ToInt16(EndDate.Substring(5, 2));
            Int16 EndDay = Convert.ToInt16(EndDate.Substring(8, 2));

            DateTime StartDateTime = persia.ToDateTime(StartYear, StartMonth, StartDay, 0, 0, 0, 0);
            DateTime EndDateTime = persia.ToDateTime(EndYear, EndMonth, EndDay, 0, 0, 0, 0);

            TimeSpan Difference = EndDateTime - StartDateTime;

            int Days = Difference.Days;
            int years = Days / 365;
            return years;
        }

        public static class CustomDateTime
        {

            public static PersianCalendar Persian = new PersianCalendar();
            private static PersianDateTime pdt = new PersianDateTime(DateTime.Now);

            #region [ Public Method(s) ]

            /// <summary>
            /// تاریخ
            /// </summary>
            /// <param name="datetype">نوع تاریخ</param>
            /// <returns></returns>
            public static string GetShamsiDate(Enumerations.DateType datetype)
            {
                switch (datetype)
                {
                    case Enumerations.DateType.Short:
                        return pdt.Year + "/" + String.Format("{0:00}", pdt.Month) + "/" +
                               String.Format("{0:00}", pdt.Day);
                        break;
                    default:
                        return pdt.Year + "/" + String.Format("{0:00}", pdt.Month) + "/" +
                               String.Format("{0:00}", pdt.Day);
                }
            }

            /// <summary>
            /// تبدیل تاریخ شمسی به میلادی
            /// </summary>
            /// <param name="ShamsiDate">تاریخ شمسی</param>
            /// <returns></returns>
            public static DateTime ShamsiToMiladi(string ShamsiDate)
            {
                int year = Int32.Parse(ShamsiDate.Substring(0, 4));
                int month = Int32.Parse(ShamsiDate.Substring(5, 2));
                int day = Int32.Parse(ShamsiDate.Substring(8, 2));
                DateTime dt = Persian.ToDateTime(year, month, day, 0, 0, 0, 0);
                return dt;
            }

            public static string MiladiToShamsi(DateTime MiladiDate)
            {
                PersianCalendar shamsi = new PersianCalendar();
                DateTime sh;
                string strdate = null;
                strdate = MiladiDate.ToString("yyyy/MM/dd");
                sh = DateTime.Parse(strdate);
                string ysh = String.Format("{0:00}", shamsi.GetYear(sh));
                string msh = String.Format("{0:00}", shamsi.GetMonth(sh));
                string dsh = String.Format("{0:00}", shamsi.GetDayOfMonth(sh));
                var a = shamsi.GetDayOfWeek(sh);
                return ysh + "/" + msh + "/" + dsh;
            }

            public static bool IsValidDate(string date)
            {
                Regex pattern = new Regex("^\\d{ 4 } /\\d{ 2}/\\d{ 2}$");
                Regex[] arrPattern = new Regex[]
                {
                    new Regex("^\\d{4}/\\d{2}/\\d{2}$"),
                    new Regex("^\\d{ 4 } /\\d{ 2}/\\d{ 1}$"),
                    new Regex("^\\d{ 4 } /\\d{ 1}/\\d{ 2}$"),
                    new Regex("^\\d{ 4 } /\\d{ 1}/\\d{ 1}$"),
                    new Regex("^\\d{ 2 } /\\d{ 2}/\\d{ 2}$"),
                    new Regex("^\\d{ 2 } /\\d{ 2}/\\d{ 1}$"),
                    new Regex("^\\d{ 2 } /\\d{ 1}/\\d{ 2}$"),
                    new Regex("^\\d{ 2 } /\\d{ 1}/\\d{ 1}")
                };
                int kabise = 1387;
                int year = 0;
                int mounth = 0;
                int day = 0;
                bool flag = false;
                for (int i = 0; i < arrPattern.Length; i++)
                {
                    if (arrPattern[i].IsMatch(date))
                        flag = true;
                }
                if (flag == false) return flag;
                //جدا کننده تاریخ می تواند یکی از این کاراکترها باشد
                string[] splitDate = date.Split('/', '-', ':');
                year = Convert.ToInt32(splitDate[0]);
                mounth = Convert.ToInt32(splitDate[1]);
                day = Convert.ToInt32(splitDate[2]);
                if (mounth > 12 || mounth <= 0)
                    flag = false;
                else
                {
                    if (mounth < 7)
                    {
                        if (day > 31)
                        {
                            flag = false;
                        }
                    }
                    if (mounth == 12)
                    {
                        int t = (year - kabise) % 4;
                        if ((year - kabise) % 4 == 0)
                        {
                            if (day >= 31)
                                flag = false;
                        }
                        else if (day >= 30)
                            flag = false;
                    }
                    else
                    {
                        if (day > 30)
                            flag = false;
                    }
                }
                return flag;
            }

            public static ShamsiTime GetCurrent(DateTime miladiDate)
            {
                PersianCalendar shamsi = new PersianCalendar();
                DateTime sh;
                string strdate = null;
                strdate = miladiDate.ToString("yyyy/MM/dd");
                sh = DateTime.Parse(strdate);
                string ysh = String.Format("{0:00}", shamsi.GetYear(sh));
                string msh = String.Format("{0:00}", shamsi.GetMonth(sh));
                string dsh = String.Format("{0:00}", shamsi.GetDayOfMonth(sh));
                int wsh = (int)DateTime.Now.DayOfWeek;

                return new ShamsiTime
                {
                    FullStringTime = getWeek(wsh.ToString()) + " " + dsh + " " + getMounth(msh) + " " + ysh,
                    FullNumeralTime = ysh + "/" + msh + "/" + dsh,
                    Day = dsh,
                    Month = msh,
                    Year = ysh,
                    MonthName = getMounth(msh),
                    WeekDayName = getWeek(wsh.ToString())
                };
            }

            public static string RelativeShamsiDate(DateTime yourDate)
            {
                const int SECOND = 1;
                const int MINUTE = 60 * SECOND;
                const int HOUR = 60 * MINUTE;
                const int DAY = 24 * HOUR;
                const int MONTH = 30 * DAY;
                string retrunDate = "";
                var ts = new TimeSpan(DateTime.UtcNow.Ticks - yourDate.Ticks);
                double delta = Math.Abs(ts.TotalSeconds);

                if (delta < 1 * MINUTE)
                    retrunDate = ts.Seconds == 1
                        ? "چند لحظه پیش"
                        : ts.Seconds.ToString().Replace("-", "") + " ثانیه پیش";

                if (delta < 2 * MINUTE)
                    return "یک دقیقه پیش";

                if (delta < 45 * MINUTE)
                    return ts.Minutes.ToString().Replace("-", "") + " دقیقه پیش";

                if (delta < 90 * MINUTE)
                    return "یک ساعت پیش";

                if (delta < 24 * HOUR)
                    return ts.Hours.ToString().Replace("-", "") + " ساعت پیش";

                if (delta < 48 * HOUR)
                    return "دیروز";

                if (delta < 30 * DAY)
                    return ts.Days.ToString().Replace("-", "") + " روز پیش";

                if (delta < 12 * MONTH)
                {
                    int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                    return months <= 1 ? "یک ماه پیش" : months.ToString().Replace("-", "") + " ماه پیش";
                }
                else
                {
                    int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
                    return years <= 1 ? "یک سال پیش" : years.ToString().Replace("-", "") + " سال پیش";
                }

            }

            #endregion
        }

        public static string getMounth(string month)
        {
            switch (month)
            {
                case "01":
                    return "فروردین";
                case "02":
                    return "اردیبهشت";
                case "03":
                    return "خرداد";
                case "04":
                    return "تیر";
                case "05":
                    return "مرداد";
                case "06":
                    return "شهریور";
                case "07":
                    return "مهر";
                case "08":
                    return "آبان";
                case "09":
                    return "آذر";
                case "10":
                    return "دی";
                case "11":
                    return "بهمن";
                case "12":
                    return "اسفند";
                default:
                    return "تعریف نشده";
            }
        }

        public static string getWeek(string week)
        {
            switch (week)
            {
                case "6":
                    return "شنبه";
                case "7":
                    return "یک شنبه";
                case "1":
                    return "دوشنبه";
                case "2":
                    return "سه شنبه";
                case "3":
                    return "چهارشنبه";
                case "4":
                    return "پنج شنبه";
                case "5":
                    return "جمعه";
                default:
                    return "تعریف نشده";
            }
        }


        public static Boolean DeleteFile(string FilePath)
        {

            if (System.IO.File.Exists(FilePath))
            {
                System.IO.File.Delete(FilePath);

                return true;

            }

            return false;


        }




        public static string GenerateFileName(string fileName)
        {
            string strfileExtension = System.IO.Path.GetExtension(fileName).ToLower();
            string strFileNewName = fileName.Replace(strfileExtension,"").Replace(" ", "-");

            return string.Format("{0}{1}{2}{3}", strFileNewName, DateTime.Now.Minute, DateTime.Now.Millisecond,
                strfileExtension);
        }


        public static Boolean SaveFile(string strFilePath, System.Web.UI.WebControls.FileUpload objFileUpload,string FileName,
            string strallowedExtensions)
        {
            Boolean IsExtensionAllowed = false;
            if (objFileUpload.HasFiles)
            {
                string fileExtension = System.IO.Path.GetExtension(objFileUpload.FileName.ToString()).ToLower();

                string[] allowedExtensions = strallowedExtensions.Split(Convert.ToChar(","));


                for (int i = 0; (i <= (allowedExtensions.Length - 1)); i++)
                {
                    if ((fileExtension == allowedExtensions[i]))
                    {
                        IsExtensionAllowed = true;
                    }

                }
                if (IsExtensionAllowed)
                {

                    bool exists = System.IO.Directory.Exists(strFilePath);

                    if (!exists)
                        System.IO.Directory.CreateDirectory(strFilePath);



                    objFileUpload.PostedFile.SaveAs((strFilePath + FileName));
                    return true;


                }
                else
                {
                    return false;
                }

            }
            return false;
        }



    }

}